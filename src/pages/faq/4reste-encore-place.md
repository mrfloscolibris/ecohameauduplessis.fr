---
title: "Reste t'il encore de la place ?"
date: "2019-04-16T22:12:03.284Z"
layout: post
type: faq
path: "/faq/reste-encore-place/"
categories:
 - Projet
---

Il y a actuellement 26 parcelles achetées sur les 28. Il reste donc 2 parcelles qui sont encore la propriété de l'association syndicale libre.

En parallèle 4 foyers souhaitent revendre leur parcelle, car leur projet a changé ou car ils n'ont pas suffisamment de moyens d'investir.

Au total ce sont donc 6 parcelles disponibles à ce jour.

Le processus d'intégration passe par une première rencontre sur place, puis la rédaction d'une lettre de motivaiton qui sert pour un entretien avec au moins deux membres du comité de pilotage, avant une décision de l'ensemble du comité de pilotage.

Pour candidater, contactez-vous via la [page Contact](/contact/).
