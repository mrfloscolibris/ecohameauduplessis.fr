---
title: "Qu'est-ce que l'écohameau du Plessis"
date: "2019-04-19T22:12:03.284Z"
layout: post
type: faq
path: "/faq/qu-est-ce-que-l-ecohameau/"
categories:
 - Définitions
---

L'écohameau du Plessis est le lieu de vie situé en Eure-et-Loir qui est dimensionné pour accueillir 28 familles qui souhaitent incarner un mode de vie écologique et harmonieux. Il soutient leur intention de partage, de sobriété, de simplicité, d'autonomie, et de respect de la Nature et des cycles de la Vie.
L'écohameau s'intègre dans un ensemble plus large, qui comprend le Centre Amma - Ferme du Plessis, l'habitat participatif "les aînés du Plessis" et le projet de maraîchage bio des Jardins du Plessis.

Pour en savoir plus, [découvrez le Projet de l'écohameau](https://ecohameauduplessis.fr/le-projet/).
