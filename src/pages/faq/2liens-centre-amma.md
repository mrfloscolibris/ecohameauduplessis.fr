---
title: "Quels liens avec le Centre Amma - Ferme du Plessis ?"
date: "2019-04-17T22:14:01.284Z"
layout: post
type: faq
path: "/faq/liens-centre-amma/"
categories:
 - Projet
---

Le projet a été initié par des membres du projet, et notamment Mathieu Labonne, à l'origine de de l'écohameau et en parallèle coordinateur du Centre Amma. Il a été réfléchi au départ pour permettre à des personnes impliquées au Centre Amma - Ferme du Plessis de résider près du Centre car le terrain de l'écohameau se situe à moins de 200 mètres du Centre.
Néanmoins les deux projets sont juridiquement et financièrement complètement distincts et tous les habitants de l'écohameau sont libres d'être intéressés ou non par les activités du Centre Amma.

En parallèle, nous cherchons cependant à créer des synergies entre les deux projets. Le Centre Amma a permis l'existence de l'écohameau en rendant de nombreux services au départ. Nous essayons dès que possible de trouver des façons de coopérer de façon équilibrée, par exemple en mutualisant des équipements.
