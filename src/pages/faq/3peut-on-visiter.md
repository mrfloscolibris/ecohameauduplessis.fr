---
title: "Peut-on visiter le lieu ?"
date: "2019-04-16T22:12:03.284Z"
layout: post
type: faq
path: "/faq/peut-on-visiter/"
categories:
 - Projet
---

Nous recevons de plus en plus de demandes de visite. Nous avons désormais planifié une visite guidée gratuite par mois.
Il est possible d'organiser une visite du lieu à une autre date mais elle serait alors payante.
Le lieu est encore en construction mais déjà habité et il est donc nécessaire de prévenir pour venir sur place.

Pour nous contacter et connaître les dates des prochaines visites, rendez-vous [sur la page Contact](/contact/) ou sur le [formulaire d'inscription](https://docs.google.com/forms/d/e/1FAIpQLSc-W5f882bCTGVao8CWA3elNz-d5TywcyCTVbHDyy_0c6qAqQ/viewform).
