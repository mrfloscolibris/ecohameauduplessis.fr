---
title: "Pourquoi un format juridique en ASL ?"
date: "2019-04-18T22:12:03.284Z"
layout: post
type: faq
path: "/faq/pourquoi-asl/"
categories:
 - Définitions
---

Nous avons choisi de monter ce projet en association syndicale libre, qui se rapproche donc d'un lotissement.
Beaucoup d'habitats participatifs et d'oasis se montent en propriété collective. Mais la taille de notre projet (28 familles) et la diversité dans les choix de construction (autoconstruction partielle, totale ou par des entreprises uniquement) rendaient très compliqués d'envisager un mécanisme de propriété collective.

Nous avons donc choisi le format d'ASL qui nous convient bien et permet de belles économies financières. Nous avons cependant des parcelles privées toutes petites qui se limitent à la taille de la maison pour garder la très grande majorité des terrains en gestion par l'ASL en tant que bien collectif.

Pour en savoir plus, [rendez-vous sur la page projet](/le-projet).
