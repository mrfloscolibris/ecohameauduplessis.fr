---
title: "Casalez"
date: "2019-02-25T22:12:03.284Z"
layout: post
type: oasis
path: "/oasis/casalez/"
avancement: "en demande de prêt"
montant: "100 000 euros"
departement: "Hérault"
description: "Une coopérative d’habitants de 10 appartements en écoconstruction à Montpellier avec des espaces et jardins partagés et un lieu d’activités ouvert sur le quartier."
---

## Le groupe
En 2013, un particulier propriétaire voit ses grands enfants entrer dans la vie active et constate l’absurdité de vivre dans une vaste maison en partie inoccupée et les incohérences écologiques de son mode de vie. Appuyé par des réseaux d'habitats groupés, il commence à envisager la création d’un d’habitat participatif sur son terrain et de s’engager dans une démarche non spéculative, plus éthique et porteuse de sens. Il décide alors de créer l’association Casalez en 2014 et réunit d’autres personnes qui partagent son intérêt et souhaitent s'engager. Aujourd’hui, le groupe est constitué en coopérative d’habitants et comprend 18 futurs habitants, âgés de 3 à 58 ans.   

![Le groupe de Casalez](./img/Casalez-groupe.jpg)

## Le projet
Pour préfigurer le fonctionnement coopératif à venir, la maison est aujourd’hui organisée en 3 logements et un espace commun créé par des chantiers participatifs successifs.
Dans le projet à venir (premier coup de pioche estimé à l’automne 2019), une partie de cette maison sera rénovée, l’autre démolie, pour laisser place à un ensemble de 12 appartements, dont 9 en PLS (logement social), du studio au T5. L’excellence écologique de la démarche est un des piliers du projet: architecture bois-paille, chauffage au poêle à pellets, seconde peau végétale, panneaux photovoltaïques,  chauffes-eau solaires, végétalisation des espaces communs pensée sur une idée de permaculture...
L’aspect social du projet Casalez est également fondamental : loin d'être conçu comme une bulle, l’association et la coopérative travaillent de concert pour faire du projet Casalez un carrefour où idées et personnes peuvent circuler et se rencontrer. Le projet actuel inclut des espaces communs partagés : buanderie, ateliers, bassin de baignade naturel, jardin, serre, potager, et une salle commune habilitée à recevoir du public. Dans une deuxième phase, Casalez espère voir naître un projet d'oasis ressource / tiers-lieu, pour aller encore plus loin.

Plus d’information sur le site internet : [www.casalez.fr](http://www.casalez.fr/).

![Plan du projet](./img/Casalez-plan.jpg)

## Le besoin de financement
La demande auprès de la Coopérative Oasis doit compléter le prêt principal actuel. Casalez est convaincue de la valeur et des bénéfices réciproques des modes de financement éthiques et solidaires et souhaite bénéficier de l’expertise et le soutien de la Coopérative Oasis, notamment pour les aspects de permaculture et la dimension participative du projet.
Egalement, il s'agit de ne pas compromettre l’excellence écologique : ce prêt permettra à Casalez de  promouvoir les artisans locaux et les savoirs-faire et de choisir des techniques innovatrices et pionnières et des matériaux écologiques.


<div class="row">
<div class="col-md-1"></div>
<div class="col-md-10">
    <a href="../../participer/" class="btn btn-block btn-primary px-5">
      <i class="fa fa-hand-o-right"></i> Soutenez Casalez en participant à la Coopérative
    </a>
</div>
</div>
