---
title: Les visites guidées au printemps 2021
date: "2021-01-24T22:40:32.169Z"
layout: post
type: blog
path: "/visites-guidees-2021/"
image: ./visite2021.jpg
categories:
  - présentation
---

L'écohameau du Plessis se construit et de plus en plus de personnes veulent venir le visiter. Pour optimiser le temps passé par les membres du projet et regrouper les visiteurs, nous avons choisi de proposer des visites guidées à des dates précises. Elles sont en entrée libre et il est possible de faire un don à la fin. Sur d'autres créneaux, nous demanderons une participation financière en fonction du temps passé.

Voici les dates des dates des visites en entrée libre pour le printemps 2021 :
- le **dimanche 14 mars** après-midi
- le **dimanche 18 avril** après-midi
- le **dimanche 9 mai** après-midi
- le **dimanche 13 juin** après-midi

Ces visites durent 2 heures et permettent d'aborder l'histoire au projet, son organisation humaine, juridique et financière et les différentes techniques écologiques utilisées. Elles se terminent en général par un goûter au Centre Amma - Ferme du Plessis (situé à 200m) pour celles et ceux qui le souhaitent.

Pour s'inscrire il est demandé de renseigner ce [formulaire d'inscription](https://docs.google.com/forms/d/e/1FAIpQLSc-W5f882bCTGVao8CWA3elNz-d5TywcyCTVbHDyy_0c6qAqQ/viewform).
