---
title: L'écohameau, sujet d'un documentaire
date: "2022-06-01T22:40:32.169Z"
layout: post
type: blog
path: "/Documentaire-JeanYves/"
image: ./Documentaire3.JPG
categories:
  - présentation
---

Depuis quelques semaines, les activités de l’écohameau du Plessis sont suivies par la caméra de Jean-Yves Philippe.

Diplômé de la FEMIS (* École nationale supérieure des métiers de l'image et du son), Jean-Yves Philippe n’en est pas à son coup d’essai puisqu’il compte la réalisation de plusieurs documentaires. Citons par exemple « Voir avec le coeur », une [coproduction KTO/Les Films d’un Jour](https://www.ktotv.com/video/00268704/voir-avec-le-coeur), tourné en 2019.

![Documentaire](./Documentaire3.JPG)

Ce réalisateur a été très intéressé par l’émergence de l'expérience écologique et humaine que représente le projet de l’écohameau à Pontgouin.

Ce sont les rapports humains, la construction du collectif, l’intégration dans la vie villageoise que le vidéaste cherche à capter, à comprendre et à partager.

Il explique ainsi  son projet : _« La thématique, c’est celle d’une aventure unique, courageuse, singulière de par son ambition. Évidemment c’est une aventure en cours, qui n’est pas terminée, qui est en « chantier » et c’est ce qui en fait aussi son intérêt. […] Ce qui m’intéresse tout particulièrement c’est la dimension humaine, en lien avec une conscience écologique aiguë, le désir d’un autre monde « à construire » avec d’autres formes de rapports humains. »_

![Documentaire](./Documentaire1.jpg)

C’est pour ces raisons que le vidéaste a suivi les membres de l’écohameau dans leurs activités les plus diverses : réunions de travail, rencontres conviviales, chantiers collaboratifs . Il a aussi visité l’environnement proche et constaté l’implication que peuvent avoir les co-lotisseurs dans la vie locale.

A l’heure actuelle, une vingtaine d’heures ont été tournées. Au montage, il en sortira un documentaire de 52 minutes qui pourra être vu en 2023.

![Documentaire](./Documentaire2.JPG)
