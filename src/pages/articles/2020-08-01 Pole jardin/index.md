---
title: Le pôle jardins de l'écohameau
date: "2020-08-01T22:40:32.169Z"
layout: post
type: blog
path: "/pole-jardins-aout2020/"
image: ./Polejardins2.JPG
categories:
  - présentation
---

Dès la création de l'éco-hameau, le COmité de PILotage (COPIL), composé de sept membres élus par l’ensemble des membres de l’ASL, est chargé de son administration et de son organisation, notamment l'organisation des différents pôles. Dès que les premiers habitants se sont installés, il s’est avéré nécessaire de créer un pôle spécifique pour une prise en charge des espaces non bâtis.

Gati, membre du COPIL, a été nommée coordinatrice. Elle nous explique le fonctionnement de ce premier pôle et dresse un bilan des premiers mois d’existence.

![Pôle jardins](./Polejardins2.JPG)

**– Comment s’est constitué ce pôle “espaces non-bâtis” ?**

Au lendemain de l’achat du terrain couvrant 4 hectares, Shambo avait organisé la plantation d’une bande forestière et de deux vergers. Puis un groupe de travail a mis en place diverses actions telles la création de potagers, l’installation d’une pergola, etc. Mais la tâche était importante : il s’agissait de préparer l’avenir tout en organisant le quotidien (l’arrosage, le débroussaillage…). C’est ainsi que le pôle “jardins” est né officiellement le 29 avril 2020.

**– Membre du COPIL, vous avez été nommée coordinatrice. Quel est votre rôle ?**

Avant tout, il s’agissait de constituer une équipe. J’ai été rejointe par Claudine, Fanny, Lucile, Loïc, et Devaraj. Hormis la gestion de l’existant, il restait beaucoup à faire. Et tout d’abord, mettre sur pied l’organigramme. Un dessin parle mieux qu’un discours. Je vous laisse découvrir le fruit de nos réflexions.

![Pôle jardins](./Polejardins1.jpg)

D’autre part, je rapporte au COPIL les actions menées. Je dois aussi avoir son accord pour tout achat de plus de 350 euros. Cela a été le cas pour la récente acquisition d’une tondeuse débroussailleuse.

**– En quelques semaines, l’équipe compte plusieurs réalisations. Pouvez-vous en dire plus ?**

C’est aussi divers que varié. Cela va de l’organisation du fauchage au planning des récoltes de fruits rouges, du dessin d’un plan détaillé des plantations à l’arrosage des différents secteurs.

Nous avons bénéficié des compétences de Loïc qui, habitant alors à Tokyo, nous rejoignait en vidéo-conférence. Il a créé un document consultable en ligne, le cockpit, qui permet de planifier les tâches.


**– Quels ont été les moments phares de cette période ?**

Nous avons institué les ateliers du jeudi après-midi. Ce sont de bons moments de travail et de convivialité. Par exemple, nous avons ainsi désherbé les stations de phyto-épuration, repiqué 120 pieds de fraisiers donnés par la Ferme du Plessis, étalé des cartons et de la paille pour préparer les terrains, et tant d’autres choses.
Nous avons participé à l’Experience week, une semaine d’échange de savoirs : fabrication d’un poulailler mobile avec des matériaux de récupération, atelier de régénération des sols avec comparatif de plusieurs méthodes.

![Pôle jardins](./Polejardins3.JPG)

**– Quels sont les principaux problèmes que vous avez rencontrés ?**

Le manque de bras pour tous ces travaux d’échardonnage, de débroussaillage, d’arrosage… Nous regrettons la mort de quelques arbres due à la sécheresse et il nous faut réguler la végétation spontanée.

**– Suite à cette expérience, quel est votre ressenti ?**

Je ressens parfois de l'inquiétude, de l'attachement aux résultats, de l'impatience, du doute.
Par contre, je me sens bien parce que ces derniers mois ont été positifs. Nous avons bien avancé à partir de pas grand chose et ce n'était pas gagné de s'organiser. J’ai pu vraiment sentir, observer la terre, m'enraciner. J'ai confiance dans notre intelligence collective.
Je suis très heureuse que nous puissions cultiver et faire des projets en liberté et dans le respect des différences, d'être co-pionniers du défrichage de la terre et de la gouvernance.

![Pôle jardins](./Polejardins4.JPG)

**– Et pour finir, d’autres pôles vont être créés ?**

C’est déjà le cas. Pour répondre aux besoins liés à l’évolution de l'écohameau, deux autres pôles ont été créés récemment. L’un pour la conduite des travaux est coordonné par Maël. L’autre, coordonné par Mathieu, est chargé de la communication et des relations extérieures. Progressivement, la gouvernance se structure et se consolide.
