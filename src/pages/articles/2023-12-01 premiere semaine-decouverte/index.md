---
title: Premier séjour découverte à l'écohameau
date: "2023-12-01T22:40:32.169Z"
layout: post
type: blog
path: "/premiere-semaine-decouverte/"
image: ./semaine-decouverte1.jpg
categories:
  - présentation
---

Du 22 au 29 octobre 2023, nous avons organisé la **première semaine « Découverte de la vie en écohameau »**. C’est ainsi que nous avons accueilli six adultes, hébergés dans la maison commune, en leur concoctant un programme riche et varié :

 • visites de l’écohameau, du Centre Amma, de la Maison Jaune, de l’école Montessori Colombes & Colibris, de la Mutinerie Village (écolieu installé depuis 10 ans à St Victor de Buthon)

 • divers chantiers en intérieur ou au jardin

 • des temps de présentation des modèles juridiques, économiques, de Vivre Ensemble

 • un atelier sur l’éco-construction

 • sans oublier, des rencontres avec les habitants autour de goûter, dîners partagés et autres temps conviviaux

![semaine decouverte](./semaine-decouverte1.jpg)

La semaine s’est joliment terminée avec le bal folk organisé au foyer communal de
Pontgouin auquel les invités ont eu plaisir de participer, y retrouvant des membres de
l’écohameau mêlés aux villageois.

![semaine decouverte](./semaine-decouverte2.jpg)

Témoignage de Guillemette : « Merci […] à tous les habitant.e.s du hameau pour votre générosité, et bel élan à partager votre aventure collective, la manière dont vous êtes structurés, organisés et dont vous insufflez une belle énergie pour un nouveau rapport au monde. Enfin merci à tous ceux et celles qui nous ont accueillis dans leur maison pour nous faire découvrir leur univers. C'était précieux ces temps d'échange, de partage, de chantier, de découverte, de visites, d'histoire des lieux et projets, aussi ces temps de fêtes.
C'était donc un séjour très complet, riche et varié, organisé avec grand soin. »

![semaine decouverte](./semaine-decouverte3.jpg)

![semaine decouverte](./semaine-decouverte4.jpg)

![semaine decouverte](./semaine-decouverte5.jpg)
