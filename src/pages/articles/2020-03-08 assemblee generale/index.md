---
title: L'assemblée générale de mars 2020
date: "2020-03-10T22:40:32.169Z"
layout: post
type: blog
path: "/assemblee-generale-mars-2020/"
image: ./AG1.jpg
categories:
  - présentation
---

Trois fois par an, les membres de l’éco-hameau sont invités à un week-end de réunions en plénière. Un de ces week-ends sert officiellement d'assemblée générale. Les grandes orientations et décisions qui relèvent de l'ensemble du collectif sont prises à l’occasion de ce rendez-vous. C’est aussi un moment de rencontre et de partage qui permet de faire le point sur l’avancée des dossiers de chacun, de visiter les constructions en cours et passer quelques heures à travailler tous ensemble aux chantiers collectifs. La deniière réunion plénière s’est déroulée les 7 et 8 mars 2020. Une quinzaine de participants se sont retrouvés dans la maison récemment occupée par Annie et Luc tandis que d’autres membres suivaient à distance depuis Paris, l’Ardèche et... l’Inde et le Japon.

![Assemblee generale](./AG1.jpg) _Exceptionnellement cette plénière a eu lieu dans une des maisons de l'écohameau._

L’essentiel des débats a été consacré au projet de la maison commune qui doit être construite au cœur du hameau. Cette maison sera un lieu de rencontre et d’accueil qui comprendra salle commune, atelier, chambres pour les invités... Les plans ont été étudiés et il s’agissait d’envisager le montage financier de cette opération. Les discussions ont été fructueuses puisque à l’issue des deux jours, le Comité de Pilotage recevait la mission de lancer les dernières démarches auprès des fournisseurs du gros œuvre, notamment le constructeur Iso-paille qui fournira les murs en bois et paille.

![Assemblee generale](./AG2.jpg) _L'étude des comptes est toujours un moment de grande concentration._

Le Comité de Pilotage est composé de sept personnes qui gèrent pendant un mandat de deux ans les affaires courantes.
C’était d’ailleurs le moment de procéder au renouvellement de ces administrateurs. Les élections,
programmées tous les deux ans, se font selon le principe des élections sans candidats. Chaque foyer vote
pour les personnes qui répondent aux critères fixés par l’assemblée (disponibilité, compétences,
engagement, caractère, talents, etc) . Le Copil réélu est identique à l'ancien :Mathieu Labonne, Maël Delemotte, Lucile Glodt, Annie Delemotte, Dorian Spaak, Vincent Luneau et Julia Tran Thah. Ils se sont mis dès le lendemain autour de la table pour préparer l’année à venir.

![Assemblee generale](./AG4.JPG) _Aussitôt réélu, le Comité de pilotage s'est mis au travail... dans la bonne humeur._

Après un repas « auberge espagnole » partagé, l’après-midi du samedi a été consacré aux chantiers extérieurs : désherbage des stations phyto-sanitaires, ramassage de silex, débrousaillage, taille des arbres, etc.). La prochaine réunion plénière a été programmée en Juillet, au moment de la semaine Experience Week à la Ferme du Plessis.

![Assemblee generale](./AG3.jpg) _Les stations de phytoépuration ont besoin d'être régulièrement désherbées._
