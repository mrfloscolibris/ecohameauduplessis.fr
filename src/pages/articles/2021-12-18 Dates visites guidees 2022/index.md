---
title: Les visites guidées de début 2022
date: "2021-12-18T22:40:32.169Z"
layout: post
type: blog
path: "/visites-guidees-2022/"
image: ./Visite-guidee.jpg
categories:
  - présentation
---

Après une année bien riche avec une dizaine de visites guidées en tout et une pause hivernale, nous proposons plusieurs nouvelles dates en 2022. Pour optimiser le temps passé par les membres du projet et regrouper les visiteurs, nous avons effectivement choisi de proposer des visites à des dates précises.

Elles sont en entrée libre et il est possible de faire un don à la fin. Sur d'autres créneaux, nous demandons une participation financière en fonction du temps passé.

Voici les dates des dates des visites en entrée libre pour le printemps et l'été 2022 :
- le **dimanche 27 mars** à 13h30
- le **dimanche 22 mai** à 13h30
- le **dimanche 17 juillet** à 14h
- le **dimanche 11 septembre** à 14h

Ces visites durent 2 heures et permettent d'aborder l'histoire au projet, son organisation humaine, juridique et financière et les différentes techniques écologiques utilisées.

Pour s'inscrire il est demandé de renseigner ce [formulaire d'inscription](https://docs.google.com/forms/d/e/1FAIpQLSc-W5f882bCTGVao8CWA3elNz-d5TywcyCTVbHDyy_0c6qAqQ/viewform).
