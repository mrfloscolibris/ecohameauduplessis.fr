---
title: Retour en images sur l'inauguration de la maison commune
date: "2024-05-20T22:40:32.169Z"
layout: post
type: blog
path: "/retours-inauguration-maison-commune/"
image: ./fete11mai-13.jpg
categories:
  - présentation
---

Le 11 mai était une journée de fête pour l'écohameau ! Baigné par un très beau soleil printanier, nous avons accueilli environ 200 personnes pendant la journée pour différents moments de célébration et en particulier l'inauguration officielle de la Maison commune en présence de plusieurs élus.

[**Découvrez l'article de l'Echo républicain sur cette journée**](https://www.lechorepublicain.fr/pontgouin-28190/travaux-urbanisme/lecohameau-du-plessis-a-sa-maison-commune_14501781/).

## Retour en 13 images sur cette journée exceptionnelle

![Fête 11 mai](./fete11mai-1.jpg) _En début d'après-midi, 3 écolieux se sont présentés aux habitants : le moulin bleu, la Mutinerie Village et les Aînés du Plessis_

![Fête 11 mai](./fete11mai-2.jpg) _L'inauguration de la maison commune a eu leiu à 16H. Mathieu, Emmy et Abel ont fait des discours ainsi que 3 élus du territoire._

![Fête 11 mai](./fete11mai-3.jpg) _Jean-François Bridet, vice-président de la Région Centre Val de Loire, a notamment expliqué pourquoi la Région avait soutenu financièrement la maison commune et l'écohameau._


![Fête 11 mai](./fete11mai-4.jpg) _Luc Lamirault, député de la circonscription, a partagé son grand intérêt à découvrir cet habitat participatif._

![Fête 11 mai](./fete11mai-5.jpg)
![Fête 11 mai](./fete11mai-6.jpg) _Pour l'inauguration, un grand circuit de dominos de bois - des pestas - a été réalisé de façon collaborative par les participants de l'inauguration._

![Fête 11 mai](./fete11mai-7.jpg) _Des visites guidées ont eu lieu pendant toute l'après-midi pour présenter le projet aux visiteurs._

![Fête 11 mai](./fete11mai-8.jpg) _Le film "l'écohameau, un chemin d'aventure", réalisé par Jean-Yves Philippe, a été projeté en fin d'après-midi dans la grande salle de la maison commune._

![Fête 11 mai](./fete11mai-9.jpg) _Plus de 130 personnes s'étaient inscrites pour le dîner en extérieur, préparé par plusieurs membres de l'édohameau. Au menu, samosas, risotto, coleslaw et pannacotta !_

![Fête 11 mai](./fete11mai-10.jpg) _Les habitants de l'écohameau et quelques voisins du territoire avaient préparé plusieurs spectacles après le dîner. Loïc a par exemple chanté avec ses filles Leïla et Naomi_

![Fête 11 mai](./fete11mai-11.jpg) _L'écohameau et le voisinage regorgent de talents musicaux qui ont pu s'exprimer pendant un spectacle joyeux devant la maison commune._

![Fête 11 mai](./fete11mai-12.jpg) _Un bal folk a été animé par une troupe de musiciens locaux, dont plusieurs membres de l'écohameau, qui s'entraînent régulièrement. Cercles circassiens, chappeloises, danses de l'ours et autres danses folk furent l'occasion de beaux moments de joie et communion pour tous les participants de la soirée._

![Fête 11 mai](./fete11mai-13.jpg) _la soirée s'est terminée tard, avec le sentiment que cette journée augurait de nombreux moments festifs et chaleureux pour cette maison commune._
