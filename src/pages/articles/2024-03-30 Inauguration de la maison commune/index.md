---
title: Le programme de l'inauguration de la maison commune
date: "2024-03-30T22:40:32.169Z"
layout: post
type: blog
path: "/inauguration-maison-commune/"
image: ./Flyer-inauguration.jpg
categories:
  - présentation
---

L’Ecohameau du Plessis est un habitat participatif et écologique qui vise à accueillir 28 foyers sur 4 hectares de terrain. 13 maisons ont été construites à ce jour.

Au centre de l’écohameau, les habitants ont construit la « maison commune », un bâtiment collectif écologique en bois et paille de 250 m2, pour accueillir différents types d’événements et activités.

Le **11 mai après-midi et soir**, nous inaugurons cette maison commune avec des animations et une soirée festive à destination des habitants du territoire. Venez découvrir le projet à cette occasion !

**Programme de la journée**

 - 14h / 15h / 17h : Visites guidées de l’écohameau
 - 15h : Présentation d’écolieux du territoire
 - 16h : Inauguration de la maison commune, en présence d’élus
 - 17h : Goûter
 - 17h45 : Film «l’écohameau, un chemin d’aventure», en présence du réalisateur
 - 18h : Atelier cirque
 - à partir de 19h : Dîner-spectacle, bal folk et danses (sur inscription)

**Inscription**

L’entrée est libre et gratuite toute l’après-midi.
Le dîner et la soirée sont à 12€ (sur inscription, début à 19h). Demi-tarif entre 5 et 15 ans.

Pour s’inscrire ou poser vos questions : contact@ecohameauduplessis.fr

[**Télécharger le flyer de l'événement**](./Flyer-11mai.pdf).
