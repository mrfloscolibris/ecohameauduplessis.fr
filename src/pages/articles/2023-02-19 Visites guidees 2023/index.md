---
title: Les visites guidées de 2023
date: "2023-02-19T22:40:32.169Z"
layout: post
type: blog
path: "/visites-guidees-2023/"
image: ./Visite-guidee.jpg
categories:
  - présentation
---

Après la pause hivernale, nous proposons plusieurs nouvelles dates en 2023. Pour optimiser le temps passé par les membres du projet et regrouper les visiteurs, nous avons effectivement choisi de proposer des visites à des dates précises.

Elles sont en entrée libre et il est possible de faire un don à la fin. Sur d'autres créneaux, nous demandons une participation financière en fonction du temps passé.

Voici les dates des dates des visites en entrée libre pour le printemps et l'été 2022 :
- le **dimanche 2 avril** à 14h
- le **dimanche 11 juin** à 14h
- un **dimanche de septembre** (date à préciser)

Ces visites durent 2 heures et permettent d'aborder l'histoire au projet, son organisation humaine, juridique et financière et les différentes techniques écologiques utilisées.

Pour s'inscrire il est demandé de renseigner ce [formulaire d'inscription](https://docs.google.com/forms/d/e/1FAIpQLSc-W5f882bCTGVao8CWA3elNz-d5TywcyCTVbHDyy_0c6qAqQ/viewform).
