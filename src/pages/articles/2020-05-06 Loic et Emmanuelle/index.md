---
title: Une nouvelle famille dans l'écohameau
date: "2020-05-06T22:40:32.169Z"
layout: post
type: blog
path: "/loic-emmanuelle/"
image: ./EmmyLoic02.jpg
categories:
  - présentation
---

Participants à l’**[édition 2019 de l’Experience Week](https://www.etw-france.org/experience-week-2019)** organisée à la Ferme du Plessis, Emmy et Loïc ont découvert l’écohameau. Parents de deux petites filles, Leïla (4 ans ½ ) et Naomi (1 an), ils sont installés depuis sept ans à Tokyo. Alors qu’ils envisageaient leur retour en France pour adopter un mode de vie plus écologique, s’inscrire dans ce projet leur a semblé l’opportunité à saisir. Après quelques mois de réflexion, le couple a décidé d’acheter une parcelle pour y construire une maison autonome.

Emmy et Loïc se sont prêtés au jeu des questions-réponses

![Loïc et Emmanuelle](./EmmyLoic01.jpg) _Leila, Naomi, Emmy et Loic_

**– Originaires de la région lyonnaise, vous résidez depuis plusieurs années au Japon. Qu’est-ce qui vous a amené à y vivre ?**

Nous aimons les voyages, la découverte et le dépaysement. C'est pourquoi nous avons saisi une opportunité professionnelle qui nous a amenés à aller vivre au Japon. Ce pays nous convient particulièrement bien car l'exotisme est vraiment total. Insulaire, c'est une culture à part qui reste décalée du reste du monde.

![Loïc et Emmanuelle](./EmmyLoic02.jpg)

**– Ce doit être une expérience enrichissante. Mais pourquoi revenir en France ?**

En plus de six ans au pays du soleil levant, nous avons énormément appris, nous nous sommes remis en question. Nous avons revu nos valeurs, nos modes de pensée et nous avons grandi. Nous ressentons désormais le besoin de passer à l'étape suivante en vivant au plus près de nos valeurs écologiques et éducatives, et à Tokyo, nous sommes limités sur ce point. Nous rentrons donc en France car c'est notre pays où vivent nos familles et nos amis. Mais nous souhaitons aussi rejoindre ceux qui agissent pour faire évoluer le monde : plus précisément, nous ne rentrons pas en France, nous venons à l'écohameau du Plessis faire vivre pleinement nos valeurs !

![Loïc et Emmanuelle](./EmmyLoic03.jpg) _Pendant l'Experience Week à la Ferme du Plessis_

**– D’où vient votre intérêt pour l’habitat partagé et tout particulièrement celui du Plessis ?**

Nous sommes des acteurs du changement, animés par de fortes valeurs sociétales et écologiques. Nous voyons en l'habitat partagé l'opportunité de faire de grands projets ensemble : vergers, potagers, ruches, constructions, spectacles, événements... Ensemble, nous sommes plus forts ! L'écohameau du Plessis nous a séduit par les valeurs qu'il porte. De passage au centre Amma, nous avons été touché par la beauté des réalisations et l'état d'esprit qui y règne. Nous croyons en l'Amour et le service désintéressé et nous tâchons de les incarner nous même. L'influence spirituelle du centre Amma nous donne confiance dans la bonne réalisation de l'écohameau.

**– Qu’est-ce qui vous a motivé, puis décidé à faire construire dans l’écohameau ?**

L'écohameau est conçu de la manière que nous souhaitions : il y a une communauté mais chaque famille garde pleinement son "chez soi". Nous avons choisi le Free Dome car il répondait pleinement à notre vision de l'habitat : écologique, esthétique, innovante... L'aménagement intérieur, basé sur une grande salle centrale qui connecte toutes les autres pièces répondait également à nos souhaits. Nous rêvions d'une maison où chacun peut avoir son espace d'intimité mais où nous sommes heureux de nous rassembler en son coeur.

La forme idéale du Free Dôme, ses équipements, sa conception, ses matériaux (paroi, isolation, etc...) en font un habitat qui respecte l'environnement et qui est autonome en énergie. Cet habitat promet un design singulier et un confort intérieur l'hiver comme l'été. En effet les murs intérieurs en terre permettent de préserver la fraîcheur à l'intérieur de l’habitat durant les périodes chaudes mais deviennent des murs chauffants durant l'hiver grâce à l'énergie solaire. Notre version du Free Dôme d'Emmy et Loïc a été adaptée pour respecter le réglement de construction de l'écohameau.

![Loïc et Emmanuelle](./EmmyLoic05.jpg) _Le Shanti dôme_

**– Emmy est enseignante Montessori ? Apporte-t-elle un projet dans ses valises ?**

Oui, Emmy travaillera dès la rentrée 2020 dans la nouvelle école maternelle Montessori Nature de Luisant : "Les Petits Explor'Acteurs". Elle y sera enseignante et référente pédagogique. A moyen terme, Emmy a le souhait d'ouvrir une école alternative à proximité de l'écohameau et construire un projet pédagogique passionnant pour les enfants.

![Loïc et Emmanuelle](./EmmyLoic07.jpg) _Emmy rejoint un projet Montessori en Eure-et-Loir_

**– Loïc est ingénieur chez Ciel & Terre, une société qui développe le solaire. Envisage-t-il des réalisations au sein de l’écohameau ?**

Oui, c'est une très bonne idée. Loïc sera ravi de s'investir dans un projet pour l'écohameau et le centre Amma. Loïc croit que le solaire peut valoriser les toitures et les ombrières des parkings de l'écohameau et il serait intéressé d'agir pour développer l'autonomie énergétique.

![Loïc et Emmanuelle](./EmmyLoic08.jpg) _Loïc est passionné par les énergies alternatives_

**– Tous les deux, vous possédez des talents artistiques.**

Emmy est chanteuse, compositrice, Loïc aussi dans un style différent. Nous jouons de la guitare et serons enchantés de partager des moments artistiques ensemble !

![Loïc et Emmanuelle](./EmmyLoic09b.jpg)

**– Et pour conclure ?**

Nous sommes très enthousiastes de rejoindre l'écohameau. Et nous avons baptisé notre future maison “Shanti Dôme” !
