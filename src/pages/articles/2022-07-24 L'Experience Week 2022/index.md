---
title: Des activités pendant l'Experience Week 2022
date: "2022-08-02T22:40:32.169Z"
layout: post
type: blog
path: "/experience-week-2022/"
image: ./ExperienceWeek1.JPG
categories:
  - présentation
---

Depuis 2010, la ferme du Plessis accueille chaque été des dizaines de participants dans le cadre d’une expérience week organisée par GreenFriends-France, la branche écologique d'ETW-France. L'objectif est de vivre une expérience de vie communautaire et écologique tournée vers des projets concrets et des échanges de savoir-faire.

Les ateliers sont riches et divers. Ils sont proposés par les participants qui échangent leurs savoir-faire. C’est de l'éducation non-formelle, c'est-à-dire de l' "apprendre en faisant". Ces Experience Weeks intègrent généralement des projets qui permettent des réalisations concrètes et utiles qui sont le support de nombreux apprentissages, de savoir-faire comme de savoir-être et de faire-ensemble !

La proximité avec la Ferme du Plessis et l’intérêt suscité par ce rendez-vous annuel ont incité certains membres de l'ecohameau à participer et s’investir dans ces ateliers. Un des buts est de developper la coopération entre les deux sites.

Cette année, les organisateurs avaient inscrit au programme le remplissage des murs de la maison commune avec des bottes de paille. Ce chantier, réalisé entre temps, n’a pas pu accueillir les participants qui auraient ainsi découvert la méthode canadienne de construction du GREB (Groupe de Recherches Écologiques de la Baie).

![Experience Week 2022](./ExperienceWeek4.JPG)

Toutefois, d’autres ateliers ont été proposés sur le site de l’ecohameau du Plessis. Encadrée par Dominique, une équipe a réalisé l’enduit en terre d’un mur sur claustras en bambou. Tout en maniant la truelle et la taloche, les stagiaires ont pu poser mille questions au chef de chantier intarissable en conseils.

![Experience Week 2022](./ExperienceWeek5.JPG)


D’autre part, plusieurs personnes ont assemblé des toilettes sèches destinées a l’école Montessori qui va ouvrir ses portes en septembre au village de Pontgouin. Loïc avait préparé cet atelier qui a permis aux participants non seulement de réaliser concrètement un projet mais aussi de comprendre l’intérêt d’installer un tel équipement dans une structure collective.

![Experience Week 2022](./ExperienceWeek6.JPG)

![Experience Week 2022](./ExperienceWeek7.JPG)

Tout au long d’un après-midi, une trentaine de stagiaires ont visité l’ecohameau en deux groupes. Apres avoir avoir retracé l’historique du projet, Mathieu apportait les informations juridiques, financières tandis que les visiteurs découvraient les lieux sous la conduite de Luc. Cela permettait de comprendre les concepts qui ont guidé les choix techniques : élévations sans ciment, phytoépuration par exemple.

![Experience Week 2022](./ExperienceWeek1.JPG)

![Experience Week 2022](./ExperienceWeek2.JPG)

Ces échanges s’avèrent bénéfiques pour tous. Le programme de l’experience-week est enrichi par ces ateliers proposés par les habitants de l’ecohameau. D’autre part, les membres de l’écohameau rencontrent ainsi des personnes intéressées et motivées et jouent ainsi l’un de leur rôle : transmettre les savoirs.

Faut-il rappeler que des habitants actuels de l’ecohameau y sont venus après avoir découvert le lieu a l’occasion d’éditions précédentes d’Experience Week ?
