---
title: Un week-end de travail sur le vivre-ensemble
date: "2022-11-20T22:40:32.169Z"
layout: post
type: blog
path: "/vivre-ensemble-novembre-2022/"
image: ./Vivreensemble1.jpg
categories:
  - présentation
---

**La maison commune, un projet soutenu par la Coopérative Oasis**

Dès la création de l’écohameau du Plessis, était programmée la construction de bâtiments communs permettant la mutualisation d’espaces ou de services. Ainsi, dans un premier temps, une laverie a été construite et les plans d’une maison commune ont été dessinés. Initialement modeste, ce projet de maison commune a évolué en fonction des besoins qui apparaissaient. Il fallait prendre en compte l’évolution prévisible du nombre d’habitants, prévoir l’accueil de visiteurs extérieurs.

![Vivre ensemble novembre 2022](./Vivreensemble1.jpg)

Le permis de construire a été déposé pour un bâtiment plus ambitieux. Parallèlement, la hausse du coût des matériaux a obligé le collectif à revoir le budget prévu et envisager un emprunt.
Les membres de l'écohameau se sont alors tournés vers la coopérative Oasis qui propose un partenariat sur mesure, incluant apport financier et accompagnement. Le financement prend la forme d’un apport avec à taux d’intérêt faible (1 %) sur une durée de dix ans.
L’accompagnement, de plusieurs jours par an, est défini sur mesure et peut être de nature très variée (aide juridique, accompagnement humain, questions liées à l’urbanisme, à la gouvernance, etc.).
Pour l’écohameau du Plessis, il convenait de travailler sur la notion de « Vivre ensemble » et c’est dans cet esprit qu’a été signée la convention de partenariat.

![Vivre ensemble novembre 2022](./Vivreensemble2.jpg)

**Une intervention préparée par le Groupe de Travail Vivre Ensemble (GTVE)**

Le sujet a été proposé par le Comité de Pilotage. Le Groupe Travail Vivre Ensemble (GTVE) a pris contact avec Daphné Vialan, spécialisée dans l’accompagnement des eco-lieux dans les domaines du Vivre-ensemble, des relations humaines et de la gouvernance.
Forte d’une solide expérience et équipée d’outils adaptés au travail en groupe, l’intervenante a défini avec Annie, Emmy, Fanny et Laetitia le contenu des interventions. Le GTVE et Daphné ont ainsi travaillé sur la forme à donner aux échanges.

En introduction Daphné partage notamment : _« L'objectif de ce we est de prendre soin des relations et du vivre-ensemble au sein de l'écohameau. Pour ce faire, nous travaillerons la notion de conflit, de manière théorique et pratique, et irons creuser avec la carte des polarités et le Processwork*, le tout en visitant trois échelles : l'individuel, l'interpersonnel et le structurel. Nous prendrons les sujets qui seront vivants dans votre groupe, et nous avons déjà identifié qu'il pourra être intéressant d'aller travailler les différences de conditions qui peuvent créer un sentiment d'inéquité.»_

![Vivre ensemble novembre 2022](./Vivreensemble4.jpg)

**Des journées de formation constructives**

La quasi totalité des membres de l’écohameau ont participé ainsi aux journées des 22 et 23 octobre 2022. Pendant deux jours, se sont succédés non seulement ateliers et expression mais aussi moments de convivialité et de détente. Toujours avec le but de mieux se connaître soi-même, de mieux connaître son voisin et le groupe dans son ensemble. Certains points de friction ont été abordés, ce qui a permis d’entrevoir des solutions.
D’un avis général, ces moments privilégiés sont à renouveler. Ils participent à la construction et à l’évolution du projet.

![Vivre ensemble novembre 2022](./Vivreensemble5.jpg)

_* Le Processwork, une approche inclusive pour traiter les conflits. C’est une méthode très créative de résolution de conflit, qui permet de laisser s’exprimer tous les points de vue et de débattre tout en avançant vers des points de résolution en s’ouvrant à plusieurs niveaux de compréhension. (La maison du processwork)_
