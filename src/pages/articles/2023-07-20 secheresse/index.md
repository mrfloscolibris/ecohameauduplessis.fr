---
title: L'écohameau face à la sécheresse
date: "2023-07-20T22:40:32.169Z"
layout: post
type: blog
path: "/secheresse/"
image: ./secheresse5.JPG
categories:
  - présentation
---

**Une sécheresse annoncée**

Le constat est inquiétant. Il est confirmé que, en ce début d’année 2023, les records de jours sans pluie ont été battus. Le changement climatique modifie déjà le cycle de l’eau. Les épisodes de sécheresse sont de plus en plus fréquents et débutent plus tôt dans l’année. Les rares pluies ont humidifié les terrains en surface mais le niveau des nappes phréatiques reste bas.

**Anticipation et prévention**

La gestion de l’eau était, dès la conception du projet, une préoccupation des constructeurs de
l’écohameau du Plessis.
Tout d’abord, il a fallu se soumettre à la réglementation qui oblige la création d’un bassin de rétention proportionné à la taille du projet qui s’étend sur quatre hectares. Le bassin proprement dit couvre une surface de 2500 m2 mais ne peut pas constituer une réserve d’eau.

![Sécheresse](./secheresse1.JPG)

D’emblée, dans le cahier des charges, il était stipulé que les maisons seraient équipées de toilettes sèches. C’est ainsi qu’aucune eau noire n’est produite et que les eaux grises sont collectées pour être dirigées vers une des trois stations de phyto-épuration. Cet équipement a été construit par les habitants et les inscrits à un chantier participatif en 2019. A la sortie de ces stations, l’eau poursuit sa course par les noues vers la mare.

![Sécheresse](./secheresse2.jpg)

Ce traitement permet de maintenir une humidité constante sur le trajet de l’eau. Pour que les eaux rejetées soient de qualité « rivière », un soin constant est apporté aux plantes qui, par leurs racines, filtrent l’eau. Cet entretien régulier est effectué chaque hiver par les membres de l’écohameau.

![Sécheresse](./secheresse3.jpg)

**Récupération des eaux de pluie**

Dans le plan d’ensemble, les eaux de pluie sont dirigées vers quatre cuves de 10000 ou 15000 litres situées sous les placettes. La laverie est approvisionnée par une eau qui y est pompée pour les lessives. La laverie est, comme les maisons, raccordée au système de phytoépuration pour l’évacuation. Les autres cuves fournissent l’eau d’arrosage pour les jardins et vergers.

![Sécheresse](./secheresse4.JPG)

A cela, certains constructeurs ont ajouté une récupération des eaux de pluie qui sont utilisées pour les besoins domestiques. Cela nécessite la pose de cuves personnelles. « Pour information, notre cuve fait 7500 litres. Elle fournit l'eau de deux foyers. » explique Clarisse. C’est le cas aussi pour la maison d’Emmy et Loic au dessin futuriste. Cette conception, négligée par les premiers constructeurs, est devenue maintenant courante. Et il n’est jamais trop tard pour envisager la mise en place de récupérateurs. Cette récupération des eaux de pluie apporte des contraintes. Cela nécessite l’installation et l’entretien d’un système de pompes et de filtres qui crée parfois des surprises. Il peut sembler plus facile de se contenter d’un branchement au réseau mais la démarche ne serait pas complète.

![Sécheresse](./secheresse5.JPG)

Toutes ces installations techniques n’apporteraient cependant pas de solution si on négligeait un comportement responsable individuel de chaque habitant.

**Des actes quotidiens conscients**

Conscients de la situation préoccupante de sécheresse, les éco-habitants adoptent les gestes élémentaires d’économie. La consommation des foyers est une affaire personnelle qui a un impact sur l’environnement. Cette auto-restriction volontaire passe par les gestes quotidiens de sobriété et par des choix. Ainsi, les jardins sont cultivés selon des méthodes de permaculture. Le sol est paillé, ce qui évite l’évaporation. Les arrosages sont méticuleux et on n’intervient sur les jeunes arbres qu’en cas de nécessité.

![Sécheresse](./secheresse6.jpg)

A l’automne, on dresse le constat. Globalement, les réserves prévues suffisent à pourvoir aux besoins de l’éco-hameau. La consommation d’eau du réseau est faible et le bilan est positif. Des pistes restent à explorer pour réduire encore les besoins.

Par notre attitude personnelle, nous pouvons contribuer au retour vers un équilibre global du cycle de l’eau.
