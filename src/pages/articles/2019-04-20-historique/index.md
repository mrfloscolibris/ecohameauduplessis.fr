---
title: "L'histoire de l'écohameau"
date: "2018-04-20T22:12:03.284Z"
layout: post
type: blog
image: ./reunion.jpg
path: "/historique-de-l-ecohameau/"
categories:
  - entretien
---

*Entretien avec Mathieu Labonne, à l'origine de l'écohameau du Plessis, sur l'histoire du projet et ses fondations.*

![Interview de Mathieu Labonne](./Mathieu.jpg)

* * *

**– En quelques mots, comment est né l'écohameau du Plessis ?**

Dès 2006-2007, l'idée de créer un lieu collectif proche de la Ferme du Plessis était présente. J'avais notamment été marqué par les trajets en voiture que beaucoup de bénévoles du Centre Amma étaient obligés de faire plusieurs fois par jour pour venir au Centre.
Au fil des années de nombreuses familles se sont en effet installées dans les environs du Centre et elles se sont bien intégrées, mais **plusieurs souhaitaient organiser un mode de vie plus en cohérence avec leurs valeurs**, notamment en terme d'écologie et de partage.

**– Comment avez-vous pu avoir un terrain de plus de 4 hectares si proche de la Ferme du Plessis ?**

En 2010, nous avions déjà commencé un groupe embryonnaire et avions visité ensemble un autre lieu sur la commune de Pontgouin. Mais il n'avait pas convenu. Le terrain de l'écohameau était auparavant réservé par un promoteur immoblier pour créer un lotissement de plus de 40 maisons.
Quand cet aménageur s'est retiré, nous avons tout de suite initié des discussions avec Francis Bossi, le propriétaire, qui était aussi le propriétaire de la Ferme du Plessis avant l'achat en 2002 par l'ONG d'Amma. Au fil des années, Francis est devenu un ami pour beaucoup de bénévoles engagées à la Ferme du Plessis. Il a été d'une grande patience car nous avons pris de nombreuses années à pouvoir acheter le terrain.
C'est vraiment une **grâce d'avoir pu acquérir finalement un terrain constructible si proche du Centre Amma**. Il crée un trait d'union parfait entre le village et le Centre.

**– Pourquoi cela a t'il été si long d'acheter le terrain ?**

Il a surtout fallu convaincre le voisinage et en particulier le conseil municipal. Le terrain était traversé par un chemin communal, seul moyen d'accès à la parcelle. Aménager la parcelle nécessitait de pouvoir disposer du chemin et en faire une route. Il a fallu que la mairie accepte de lancer la procédure de déclassement du chemin puis de cession, en sachant que nous rétrocèderons la route créée à la commune. La préfecture a été d'un grand soutien pour trouver les bonnes modalités et expliquer aux élus l'intérêt d'un tel projet immobilier pour le village.
Au final **la situation s'est débloqué en 2017 et le permis d'aménager a pu être obtenu en septembre 2017**.

![Travaux d'aménagement](./permisamenager.JPG) _Certains des premiers habitants devant l'affichage du panneau annonçant l'aménagement de la parcelle !_

**– Comment avez-vous pu rassembler les familles**

Rapidement, nous étions une bonne douzaine à échanger sur le projet, principalement des personnes très engagées dans les activités écologiques du Centre Amma. Les années d'attente du permis d'aménager ont permis de rassembler un total de 26 familles. Notre enjeu a été de **maintenir l'engagement du groupe dans un projet qui était alors incertain** et qui ne communiquait pas ou très peu vers l'extérieur vu les incertitudes.
Les apports des 26 familles ont permis de porter le coût de l'achat et de viabilisation. Seules 2 parcelles n'ont pas été vendues dès le départ et c'est donc toujours l'ASL (association syndicale libre), qui gère les espaces collectifs, qui en est propriétaire, en attente de nouveaux acquéreurs.

**– Quelle était la dynamique du collectif ?**

Notre projet se base sur un **lien fort entre la majorité des habitants, qui sont plus ou moins engagés au Centre Amma - Ferme du Plessis**. Cela a créé une confiance essentielle. La proximité du Centre est la première des motivations des habitants. Ainsi, c'est un collectif moins exigeant que dans d'autres projets similaires. Cette confiance a permis qu'un groupe très restreint puisse gérer le projet pour le compte des autres, ce qui était nécessaire vu les complexités administratives. Rapidement, nous avons donc eu un comité de pilotage très central. Ce n'est pas très habituel pour beaucoup de groupes dans des projets similaires, où les plénières ont un rôle plus important mais risquent d'épuiser le groupe si le projet n'avance pas très vite.

**– Comment s'est fait le choix du modèle juridique ?**

Le terrain est grand et sa viabilisation est chère. Nous l'avons su assez tôt. Ainsi **il fallait être nombreux pour que le coût par foyer ne soit pas trop important**. Nous sommes peu à peu arrivés au chiffre de 28, principalement suite à un design en permaculture du lieu et à l'organisation du hameau en 5 "fleurs" de quelques maisons, entourant un potager collectif.
Nous étions donc nombreux et avec des réalités financières différentes. Certains ont les moyens de faire tout construire, d'autres font tout en auto-construction. Nous avons trouvé plus simple de partir sur un modèle de type "lotissement". C'est notre notaire et notre géomètre qui ont creusé avec nous la piste d'une association syndicale libre. Nous savons que ce format juridique, en propriété privée, a quelques limites, notamment un risque de spéculation sur le long terme, mais **sa force réside dans sa simplicité et dans une gestion peu chronophage**. Vu nos engagements au Centre Amma, dans nos vies privées et professionnelles, il fallait être réaliste sur le modèle juridique.
Ce format a finalement de multiples avantages, notamment la prise en compte des temporalités, des souhaits individuels et l'allégement de certains coûts et difficultés, comme l'accès à de l'emprunt bancaire ou les questions d'assurance pour la construction.
Enfin, **la présence d'un règlement de construction permet quand même d'encadrer les choix individuels**, pour assurer une cohérence globale.

**– Et justement pour les règles de construction, comment êtes-vous arrivés à vous mettre d'accord sur le cahier des charges des maisons ?**

La rédaction de ce document a été un long cheminement. Encore après l'obtention du permis d'aménager, nous l'avons fait évoluer car nous avons découvert certains manquements.
Il s'est construit sur la base de nos souhaits d'auto-construction, tout en veillant à rester réaliste.
Une petite anecdote par exemple concerne les toilettes sèches. Dans les débuts, nous n'avions pas prévu d'obliger des toilettes sèches, car nous pensions qu'anticiper un tel choix pour 28 familles serait compliqué. Mais en visitant le Hameau des Buis, en Ardèche, nous avons compris que ce choix nous permettrait de réaliser une phytoépuration, et ainsi de prendre responsabilité sur le traitement de nos eaux usées. Finalement, l'option est venue sur la table, et à notre grand étonnement, toutes les familles engagées ont été favorables au choix des toilettes sèches.
Ce règlement est notre garde-fou et un des moyens de s'assurer que les habitants partagent certaines valeurs écologiques. Il est essentiel de **savoir garder un équilibre entre une cohérence globale et de vrais modalités de souveraineté individuelle**.
