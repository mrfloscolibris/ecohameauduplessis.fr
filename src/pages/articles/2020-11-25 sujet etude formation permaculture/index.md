---
title: L'écohameau comme sujet d’étude pour une formation en permaculture
date: "2020-11-25T22:40:32.169Z"
layout: post
type: blog
path: "/sujet-formation-permaculture/"
image: ./permaculture1.JPG
categories:
  - présentation
---

C’est devenu désormais un rendez-vous devenu incontournable. Chaque année, depuis 2012, Norbert Fond et toute une équipe donnent rendez-vous pendant 15 jours aux personnes motivées par la permaculture. Cette formation reconnue, initiée par la branche GreenFriends de ETW-france, est hébergée au Centre Amma  à la ferme du Plessis, à moins de 200 mètres de l’éco-hameau.

Le lieu est doublement propice cette année puisque les réunions, les conférences et des ateliers se tiennent dans les salles du Centre tandis que les stagiaires se voient confier l’étude d’un projet sur les quatre hectares du hameau tout proche. Cela permettra de compléter le design déjà réfléchi depuis plusieurs années, qu'il faut constamment savoir requestionner.

Les organisateurs devaient toutefois surmonter cette année des obstacles générés par la pandémie de la COVID19. D’une part, le groupe devait s’adapter aux mesures sanitaires en vigueur. D’autre part, Warren Brush, le spécialiste américain reconnu qui intervient à chaque session, ne pouvait faire le déplacement. Ses interventions ont donc été faites en vidéo-conférence. Co-fondateur de l’écovillage Quailsprings où il  réside en Californie, Warren a suivi l’évolution de l’étude menée par les participants et a donné des conférences introductives, rappelant notamment avec de nombreuses histoires les trois points de l’éthique en permaculture :
 - Prendre soin de la terre
 - Prendre soin des humains
 - Redistribuer équitablement les surplus

 ![Design en permaculture](./permaculture1.JPG)

D’autres activités ont dû être adaptées. Par exemple, la visite prévue à la ferme du Bec-Hellouin a été remplacée par une vidéo-conférence animée par Perrine Hervé-Gruyer qui en est la co-fondatrice de ce lieu expérimental en permaculture avec son mari Charles.

**La permaculture est d’abord un projet politique qui cherche des solutions concrètes pour organiser un monde différent.**

Le sujet de design correspondait tout à fait à la demande exprimée par les habitants actuels ou futurs de l’éco-hameau. Loïc Leruste a donné les résultats d’une enquête menée auprès des co-lotisseurs où chacun partageait sa vision de l’organisation de l’espace commun. Y étaient cités, en vrac, les plantations de haies, la création de mares, l’élévation de serre semi-entérée, construction d’un poulailler, etc.

A cette liste de desidératas, s’ajoutaient les contraintes du terrain et du climat, le dessin déjà établi du plan de quartier et autres données qu’il restait à découvrir. Les stagiaires devaient alors plancher par groupe de 5 ou 6 sur une situation concrète, tout en respectant les principes fondamentaux de la permaculture. Trois éco-lotisseurs étaient inscrits à cette formation, ce qui permettait un contact direct.

![Design en permaculture](./permaculture2.JPG)

**Se rendre sur le terrain pour se projeter**

Ce programme devait être monté en quelques jours. Pour cela, le groupe s’est rendu sur les lieux pour arpenter le terrain et rencontrer les membres de l’éco-hameau.

Ainsi Hervé et son groupe se sont présentés avec l’intention de « planter l’eau », expression énigmatique qui signifie étudier la meilleure gestion de l’eau : capter les eaux de pluie pour les dériver vers des réservoirs utilisables en saison sèche, suivre les noues existantes. Déjà, les eaux de pluie sont récupérées pour être dirigées vers les laveries communes. Mais on a constaté un déficit pour l’arrosage des jardins lors du dernier été très sec.

![Design en permaculture](./permaculture3.JPG)

Pour Nicolas et ses collègues, ce sont les problèmes liés aux vents et courants d’air qui pourraient être résolus par la plantation de haies.

Patricia, elle, s’étonnait de ne pas trouver le coeur de l’éco-hameau.
« Les choses évoluent doucement parce que tout se fait avec la contribution volontaire des membres » lui explique Loïc. « La semaine dernière, en petite équipe, nous préparions la terre des placettes situées au centre de chaque îlot pour y semer de l’engrais vert. »

Un des participants a tout particulièrement apprécié les remarques émises lors de ces journées. Il s’agit de Thomas Desaire qui, associé à sa femme Magguy, cultive en maraîchage les terrains attenant à la ferme du Plessis. « Je découvre cette vision de la culture avec émerveillement » déclare le maraîcher qui compte bien mettre en pratique les principes enseignés.

![Design en permaculture](./permaculture5.JPG)

Pour la petite histoire, sur le terrain, Steve a découvert le poulailler mobile maintenant habité par cinq gallinacées. Il avait participé à la réalisation de cet équipement pendant l’Experience Week organisée cet été 2020 sur ce même lieu.

**Une restitution avec une suite attendue**

Et ainsi, chacun a pu apporter sa contribution au travail d’équipe. Chacune a planché sur un projet d’où ont surgies de riches idées. Les panneaux explicatifs, reprenant le plan cadastral initial donnaient une vision claire qui a été restitué en fin de formation devant l’ensemble des participants. Depuis la Californie, Warren Brush suivait et commentait les présentations.

Dans l’assemblée, plusieurs membres de l’éco-hameau ont accueilli ces idées avec beaucoup d’intérêt. Dans les jours qui ont suivi, le pôle jardin a repris les plans pour en tirer les leçons et réfléchir à la réalisation de quelques travaux.

Après la remise des certificats, Norbert Fond a exprimé sa gratitude et a souligné le sérieux du travail effectué pendant ces deux semaines intensives.

« J’ai  apprécié l’esprit de coopération qui a été développé au sein du groupe.» explique le formateur. Pour lui, il ne fait aucun doute qu’une prochaine session sera organisée l’an prochain. Nous pourrons alors voir ce qui a été mis en œuvre et les conséquences bénéfiques sur la vie de l’éco-hameau du Plessis.

Les idées émises pendant cette session ne sont d'ailleurs pas restées sans suite puisque fin novembre le merlon ouest était végétalisé à l’occasion d’un chantier collectif qui a réuni les habitants de l’écohameau. De la même façon, les jardins vont être protégés par des haies de fagots. Le paysage aura déjà continué d'évoluer suite à ces aménagements.

![Design en permaculture](./permaculture4.jpg)
