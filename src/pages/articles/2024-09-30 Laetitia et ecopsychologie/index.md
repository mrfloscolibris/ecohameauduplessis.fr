---
title: Laetitia, habitante qui s'intéresse à l'écopsychologie
date: "2024-09-30T22:40:32.169Z"
layout: post
type: blog
path: "/laetitia/"
image: ./laetitia1.jpg
categories:
  - présentation
---

**Diversité et complémentarité**

L'écohameau du Plessis est un lieu où coexistent une grande diversité de métiers et de profils, des charpentiers, des ingénieurs, des maraîchers, des professionnels de santé, des artistes, des professeurs, et des retraités. Cette diversité contribue à l'enrichissement de la communauté, où chacun apporte sa contribution pour le bien commun. Cette variété de savoir-faire favorise l'entraide et les échanges, essentiels pour le projet collectif.

Laetitia, professionnelle de santé, s'est investie dans le projet d'écohameau depuis ses débuts. Après plusieurs années de travail, sa maison a été construite avec l'aide d'amis et de professionnels du bâtiment. Elle a participé activement aux finitions et à l'aménagement de sa maison, en optant pour des meubles chinés sur des sites de seconde main, contribuant ainsi à la réutilisation et à la durabilité des matériaux.

![Laetitia](./laetitia1.jpg) _Pendant la construction de sa maison, Laetitia explique ses choix de techniques écologiques._

**Laetitia, psychologue, à l’écoute et au service des autres**

En tant que psychologue, Laetitia travaille à 10 km de l'écohameau dans une maison de santé où elle propose une approche conventionnelle basée sur la thérapie cognitive comportementale (TCC) pour traiter les troubles anxieux et prévenir les rechutes dépressives. Elle s'est aussi formée à la méditation pleine conscience pour aider les personnes souffrant de troubles de l'attention. Plus récemment, elle a créé une association de santé mentale globale pour promouvoir une approche intégrée de la santé reprenant ainsi les modèles corps-mental-âme des traditions ayurvédique ou chinoise. En France, ce modèle unifié s’organise sous le terme de la vision bio-psycho-sociale et spirituelle de la personne et Laetitia travaille en collaboration avec d’autres professionnels tels que des spécialistes de la nutrition, des médecins psychiatres, des praticiens de l'Ayurveda et des praticiens en sport adapté.

Au sein de l'écohameau, Laetitia s'est investie dans la sensibilisation à l'éco-anxiété, un sujet moderne qui affecte de nombreuses personnes face aux défis environnementaux actuels et qui souligne l’aspect systémique des relations au vivant. Lors d'une [semaine-découverte](https://www.ecohameauduplessis.fr/la-maison-commune/), elle a par exemple animé un atelier pour expliquer comment ces préoccupations écologiques peuvent provoquer des troubles anxieux, et a mis en avant l'importance de reconnaître ces sentiments et d'y répondre par des actions concrètes en faveur de l'environnement, telle que la technique des « petits pas ».

![Laetitia](./laetitia2.JPG) _Laetitia, le jour de la signature de l'achat de sa parcelle en 2018_

**Qu’est-ce que l’écopsychologie ?**

Laetitia a publié un **[article sur l'écopsychologie, abordant la relation entre l'individu et son environnement](https://champsocial.com/book-psm_3-2024_nature_et_vie_psychique,1346.html#sommaire)** paru dans le magazine PRATIQUES EN SANTÉ MENTALE N°276.

Elle y explique comment les troubles de santé mentale peuvent être le reflet d'un déséquilibre de l'environnement. Le choix de vie communautaire, qui respecte les besoins individuels tout en favorisant le soutien social, est présenté comme un facteur de résilience important pour la santé mentale, permettant de surmonter la solitude et de s'engager dans l'aide à autrui. Source : https://www.minds-ge.ch/ressources/les-comportements-promoteurs-dune-bonne-sante-mentale

Le texte met également en lumière l'importance des valeurs spirituelles pour créer une cohésion et une profondeur au sein des membres du groupe. La vie communautaire est décrite non pas comme une fin en soi, mais comme un moyen pour chaque individu de grandir, d'apprendre, et de progresser sur un chemin de service, de connaissance et de respect mutuels.
