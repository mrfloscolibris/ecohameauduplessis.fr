---
title: Le chantier paille de la maison commune
date: "2022-07-15T22:40:32.169Z"
layout: post
type: blog
path: "/chantier-paille-2022/"
image: ./chantier-paille-3.jpg
categories:
  - présentation
---

**Du projet de maison commune à sa réalisation**

Comme dans de nombreux ecohameaux, les habitants ont fait le choix de rassembler plusieurs espaces de vie dans une « maison commune ». Celle-ci comportera plusieurs services : une salle polyvalente, un atelier, des chambres d’amis, un espace de travail, ainsi que l’une des laveries communes.

Au rez-de-chaussée, la salle polyvalente de 50 m2 pourra servir pour des réunions, des activités manuelles ou artistiques, des activités pour les enfants, des repas partagés… Elle pourra aussi être louée aux habitants qui proposent des formations (danse, yoga…).

Un atelier de 20 m2 sera ouvert pour bricoler et stocker une partie du matériel collectif.

Les maisons individuelles intègrent souvent une « chambre d’amis » qui sert ponctuellement et induit donc une surface à construire et à chauffer pour une utilisation limitée. Le choix a été fait de mutualiser dans cette construction des chambres à l’étage, avec salle de bains, cuisine et toilette sèche communes. Un système interne de réservation des chambres sera mis en place. Elles pourront aussi être louées pour financer les besoins de l’ASL. Au dernier ét space de rencontre pour les habitants.

Le projet ne manquait pas d’ambition et de nombreuses discussions ont été nécessaires pour que les habitants expriment leurs besoins. Un pole travaux composé de cinq membres de l’ASL est chargé du suivi des travaux, des relations avec les artisans. Un groupe de travail se penche sur les aspects pratiques, esthétiques.

![Chantier paille](./chantier-paille-4.jpg)

**De l’idée au concret**

Les travaux on débuté fin 2021. Des que les fondations ont été coulées, c’est Abel, habitant l’écohameau et menuisier-charpentier qui a été chargé de monter l’ossature bois. Selon la méthode du GREB qui a été choisie, lesmontants doivent être remplis de bottes de paille.

Prévenants, l’an passé, les membres avaient engrangé près d’un millier de bottes dénichées chez un agriculteur du coin. Ce trésor de guerre était stocké dans un hangar voisin. La matière première était sur place. Le chantier participatif programmé début juillet pouvait se mettre en place, organisé par les membres du pôle travaux.

![Chantier paille](./chantier-paille-2.jpg)

**Une expérience enrichissante pour tous**

La mobilisation a été importante et les participants étaient motivés. Le chantier qui avait été prévu sur une quinzaine de jours fut mené tambour battant. En une semaine, les murs étaient remplis de paille.

Pendant qu’une dizaine de personnes participaient au chantier, on s’activait en cuisine pour proposer chaque midi un repas pris ensemble. Ces moments conviviaux ont fortement renforcé l’esprit de groupe.

Les témoignages sont unanimes : l’esprit qui a animé les participants a été constructif. A la vue de l’avancée des travaux et considérant l’investissement des membres de l’ASL, le collectif souhaite réitérer l’expérience. Et ce ne sont pas les occasions qui manqueront.

![Chantier paille](./chantier-paille-3.jpg)
