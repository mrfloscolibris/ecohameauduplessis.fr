---
title: L'écohameau du Plessis sur France24
date: "2021-11-10T22:40:32.169Z"
layout: post
type: blog
path: "/documentaire-france24/"
image: ./grosmoteco.png
categories:
  - présentation
---

L'écohameau du Plessis a été filmé à l'automne 2021 pour le reportage de l'émission "Le gros mot de l'éco" sur le thème de la décroissance pour France24.

Découvrez la vidéo de France 24, [en cliquant ici](https://www.youtube.com/watch?v=KSsYyK1bnlE&t=304s).
<iframe width="560" height="315" src="https://www.youtube.com/embed/KSsYyK1bnlE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
