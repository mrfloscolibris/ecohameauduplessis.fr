---
title: Les enfants dans l'écohameau
date: "2023-08-25T22:40:32.169Z"
layout: post
type: blog
path: "/enfants-ecohameau/"
image: ./enfants-ecohameau1.JPG
categories:
  - présentation
---

**Trois générations se croisent**

« Mon jour préféré à l’écohameau, c’est le mercredi » déclare d’emblée Annie qui a le privilège d’être la doyenne. « Tout au long de ce jour sans école, les enfants vont et viennent, s’invitent les uns chez les autres, courent vers leurs activités extérieures. C’est très vivant ». Les relations intergénérationnelles sont une des caractéristiques notables du quartier. La cohabitation des trois générations qui y résident est source d’échanges enrichissants.

Depuis que les premières maisons ont été occupées, trois naissances sont venues renforcer les effectifs des mineurs. Actuellement, dix enfants âgés de 6 mois à 11 ans vivent déjà sur les lieux.

![Les enfants](./enfants-ecohameau1.JPG)

**L’écohameau, terrain d’aventure**

« Pour eux, c’est le paradis » explique Lucile, maman d’une fille de 7 ans et d’un garçon qui entre en 6e. « Ils ont l’espace et la liberté ». Une liberté contrôlée puisque, pour des raisons évidentes de sécurité, les enfants n’ont pas le droit de sortir seuls du périmètre imposé et de s’approcher des endroits à risques comme la mare et les chantiers.

« J’ai beaucoup aimé le cirque » annonce Nathaël qui participe aux ateliers proposés par Rémy, artiste circassien qui n’hésite pas à sortir tout son matériel pour un après-midi de jonglage et d’équilibrisme. D’autres fois, c’est Claire qui anime une séance de kung-fu ou une répétition de musique indienne. Les enfants sont les bénéficiaires des talents partagés par les adultes. Ce que préfère Gabriel, c’est le jardinage. « Je partage des graines avec la voisine et je lui ai donné de la menthe de mon jardin » raconte le jardinier en herbe.

Pour eux, les jeux et les fêtes sont aussi les occasions de vraiment « vivre ensemble ».
« Quand il y a un anniversaire, nous passons l’après-midi ensemble. Et nous invitons des copines et copains du village » détaille Nathanaël. Car pour les enfants, comme pour leurs parents, pas question de s’enfermer dans un entre-soi.
Les liens avec l’extérieur existent. La bibliothèque municipale et l’école de musique les accueillent régulièrement. L’association « Le café des enfants – La Fabrik » donne rendez-vous une fois par mois à tous, du plus jeune au plus âgé pour des activités créatives. Et bien sûr, il y a la scolarisation.
A la rentrée, deux collégiens prendront le car chaque matin vers le chef-lieu de canton tandis qu’à Pontgouin les effectifs vont se répartir entre l’école publique et « Colombes et Colibris », l’école Montessori initié par Emmanuelle et Loïc.

![Les enfants](./enfants-ecohameau4.jpg)

**Le comité des enfants**

A l’écohameau, la particularité est l’organisation que les enfants ont monté avec l’aide de Fanny, Rémy et Pierre. Ce dernier, pédagogue intéressé par les modes de fonctionnement du groupe des enfants au sein du collectif, rappelle la démarche effectuée par le groupe.
« En échangeant avec eux, nous avons compris qu’ils se posaient beaucoup de questions sur notre fonctionnement et qu’ils n’avaient pas de réelle place institutionnelle dans le projet collectif. Il manquait un cadre que nous avons dessiné ensemble en créant un comité des enfants. En se réunissant régulièrement nous avons pu répondre aux interrogations qu’ils avaient et les accompagner vers une place plus juste et responsable dans l’écohameau. Les enfants se saisissent
désormais mieux de leurs droits et devoirs au sein du collectif. Ils ont pu par exemple réfléchir à des aménagements extérieurs et adresser une proposition au pôle jardins. Chaque enfant a également pris une ou plusieurs responsabilités à sa mesure pour soutenir le collectif afin de restituer ce qu’ils ont conscience d’avoir reçu. Ainsi certains sortent et rentrent les poubelles chaque semaine, d’autres nourrissent les animaux, etc. »

![Les enfants](./enfants-ecohameau3.jpg)


Le groupe des enfants a été initié en 2023. Il regroupe les « plus de 5 ans » et invite des ami-e-s extérieur-e-s à participer aux réunions mensuelles. Le projet de cette année était ambitieux : ouvrir un restaurant mensuel pour pouvoir acheter un trampoline. « Et bien, croyez nous, nous y sommes arrivés » s’exclament les concernés. « Il a fallu adapter, « convaincre » les parents, faire les invitations et régler les mises en scène mais après trois repas (l’un indien, l’autre japonais, le dernier à l’occasion de la Fête de la Musique) ouverts aux habitants, on a fait les comptes et on a pu acheter ce grand trampoline. »

![Les enfants](./enfants-ecohameau5.jpg)

Depuis, le trampoline a rejoint la yourte prêtée et montée par Vishwan sur le terrain. Un terrain sans cesse occupé, habité et qui se transforme peu à peu en vrai terrain d’aventure.

![Les enfants](./enfants-ecohameau6.jpg)
