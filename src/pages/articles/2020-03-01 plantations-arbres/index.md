---
title: Des bénévoles taillent les arbres de l'écohameau en mars 2020
date: "2020-03-07T22:40:32.169Z"
layout: post
type: blog
path: "/taille-arbre-2020/"
image: ./plantations1.JPG
categories:
  - présentation
---


Le terrain de l’éco-hameau couvre 4 hectares qui doivent être mis en valeur et entretenus. Avant même que les maisons ne sortent de terre, un plan des espaces non-bâtis a été élaboré et des arbres ont été plantés. Ce plan définit aussi les emplacements des potagers, de la plate-forme de compost, les voies de circulation, les stations de phyto-épuration, etc.

De récentes plantations ont complété la collection d’arbres déjà mise en place : une rangée de lilas décore la voie principale. Ces végétaux ont besoin d’entretien. En ce début de mois mars, une séance de taille a été menée sous la houlette de Michel, venu apporter bénévolement ses compétences de jardinier. Entouré de quelques stagiaires, il a prodigué ses conseils qui ont été mis aussitôt en pratique.

![plantation](./plantations1.JPG) _Atelier de taille début mars avec quelques bénévoles._

Plusieurs autres chantiers ont été mis en œuvre : débroussaillage des abords, désherbage des stations de phytoépuration, nettoyage des terrains. Pour cela les membres de l’écohameau y ont travaillé ensemble à l’occasion de la réunion plénière. « Le travail réalisé en quelques heures est remarquable » remarque Annie Delemotte qui a préparé l’ensemble des actions à mener sur le terrain. « C’est la force du groupe. Se construit et se renforce ainsi la cohésion de l’équipe. » ajoute Lucile Glodt qui coordonnait l’opération « Ramassage des silex », nombreux dans le champ.

![plantation](./plantations2.jpg) _Régulièrement des membres du projet ramassent des silex sur le terrain._

Récemment occupé par plusieurs famille, le terrain a connu une fréquentation régulière qui a permis de mieux connaître la circulation de l’eau et, notamment, l’écoulement des eaux de pluies vers la mare et la zone de rétention. Des aménagements ou drainages sont à prévoir pour offrir aux plantations les meilleures conditions de développement. Quelques arbres ont été sauvé d’une asphyxie certaine depuis que des rigoles ont été creusées. D’autre part, une poche d’eau qui se formait après chaque épisode de pluie soutenue, a pu être vidée, libérant ainsi une plantation des risques d’inondation.

Des travaux de voirie et l’aménagement des abords des maisons occupées doivent compléter ce programme de régulation des eaux pluviales. Pour cela, des engins de chantier sont nécessaires et une prochaine réunion avec les entreprises concernées va permettre d’élaborer le prochain calendrier.
Un problème reste à résoudre : le passage des véhicules motorisés endommage le chemin du Plessis . Le haut de ce chemin pourra être réservé aux seuls piétons, cyclistes et cavaliers. Pour rejoindre la ferme du Plessis, les voitures et camions sont invités à redescendre vers l’entrée de l'éco-hameau, pour remonter par la rue Patton. Une réflexion sur la signalétique est engagée. Des panneaux d’orientation, gravés dans le bois, informeront les habitants et les visiteurs.
