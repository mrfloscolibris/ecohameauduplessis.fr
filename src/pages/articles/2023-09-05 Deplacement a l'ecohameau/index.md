---
title: Les déplacements de l'écohameau du Plessis
date: "2023-09-05T22:40:32.169Z"
layout: post
type: blog
path: "/deplacements-ecohameau/"
image: ./mobilite1.jpg
categories:
  - présentation
---

Le constat est évident. Près de la moitié des émissions de carbone de l’écohameau du Plessis, comme dans la grande majorité des écohameaux, vient des mobilités. C’est le poste où il y a le moins de différence avec la moyenne nationale, celui où il y a la plus grande marge d’amélioration.

![déplacements](./mobilite1.jpg)

**Localement, à pied, en vélo ou en train**

Pontgouin est un village traversé par une départementale fréquentée, situé entre Courville-sur-Eure et La Loupe où sont concentrés les services et commerces les plus indispensables. Dès qu’il s’agit de chercher un spécialiste, d’effectuer des achats spécifiques, il est nécessaire de rejoindre Chartres à 25 kilomètres.
L’existence de la voie ferrée Paris-Le Mans est un atout appréciable puisque les usagers peuvent prendre le train à la station ouverte à trois kilomètres du centre-bourg. Cette proximité permet à celles et ceux qui travaillent sur Chartres ou Paris d’emprunter facilement cette ligne. « Chaque jour, je pédale jusqu’à la gare d’où je pars vers Chartres où je suis employé » explique Maël. Mathieu utilise cette ligne pour se rendre régulièrement dans des tiers-lieux parisiens qu'il préside.
Ils espèrent la réalisation de la liaison douce qui devrait relier dans quelques mois le centre-bourg à la station ferroviaire.
La vie dans un petit village oblige ses habitants à envisager des trajets vers les centres où se regroupent commerces et services. Pour l’instant, une part importante des aller-retours vers le bourg se font à pied ou en vélo. « Chaque matin, Loïc ou moi nous rendons à l’école Colombes et Colibris avec nos deux enfants à pied ou en vélo électrique » explique Emmy. « On peut aussi facilement aller jusqu’à l’épicerie et au café associatif La Maison Jaune en empruntant le chemin pédestre qui longe le ruisseau du Royneau » précise Aurore.

![déplacements](./mobilite2.jpg)_Loic revient de l'école avec ses 2 filles_

**La bagnole.... Difficile de s’en passer**

Toutefois en zone rurale, dès qu’il s’agit de déplacement, on a souvent besoin de la voiture. Quand ont été dessinés les plans de l’écohameau, des places de parking avaient été prévues le long de l’axe central. A chaque maison correspondait un emplacement et une large place pouvait accueillir les véhicules à l’entrée du quartier.
En s’installant dans leurs nouvelles maisons, les habitants avaient gardé l’habitude de posséder leur propre véhicule. Un rapide recensement permettait d'approcher une voiture par adulte !
Il a fallu attendre un peu pour qu'on s'organise du partage de voitures et camionnettes. Loïc coordonne désormais l'usage de Coloc-auto, une application de Mobicoop qui simplifie les échanges de véhicules. « Des voitures qui ne sont pas utilisées quotidiennement sont mises à la disposition de tous, avec la clé en libre accès. Évidemment, libre choix est laissé aux habitants de participer à cette expérience.. Mais nous voulons aller plus loin et avoir de vrais véhicules communs. » explique-t’il. Côté véhicules électriques, on ne compte pour l’instant qu’une Zoé et deux vélos et le projet d’en acheter d’autres est en réflexion. Pour cette raison, l’installation d’une borne de recharge est prévue au niveau de la maison commune.

De son côté, Mathieu collecte les informations sur nos déplacements en automobile. Il note les kilométrages relevés par les propriétaires pour les transmettre à un groupe de travail de la Coopérative Oasis. Ce groupe est animé par Yoann Gruson-Daniel, ingénieur spécialisé en mobilité bas carbone et habitant dans l’habitat partagé Ecoravie dans la Drôme qui explique : « Avec sept habitats partagés du réseau Oasis*, nous avons commencé un gros travail d’acquisition de données, c’est-à-dire des relevés mensuels des kilomètres de tous les véhicules (mutualisés ou non), voire même des vélos, pour réussir à comprendre leur utilisation. [...] Ce sont des données précieuses pour se rendre compte notamment, sur quels trajets la voiture pourrait être remplacée par un autre véhicule. L’étape d’après consiste à finaliser l’analyse de ces données et à partager les bonnes pratiques.»
Ce militant du vélo électrique a rendu visite à l’écohameau du Plessis où il a exposé les solutions expérimentées à Ecoravie. « Ces échanges ouvrent des perspectives intéressantes » explique Maël qui envisage de participer à un stage d’électrification de vélos proposé par Yoann.

![déplacements](./mobilite3.jpg)_Yoann est venu présenter les solutions apportées par les véhicules électriques aux habitants_

**Le point noir : l’avion**

Inutile de cacher l’évidence : le bilan carbone de l’écohameau est plombé par l’utilisation de l’avion. Il apparaît que les habitants de l’écohameau ont beaucoup voyagé tout au long de leur vie, bien souvent par les airs. Cette pratique est aujourd’hui à juste titre remise en question mais force est de constater que, même si on note de louables efforts pour restreindre les trajets aériens, le bilan carbone de l'écohameau est déséquilibré. Le nombre de voyages, le plus souvent à destination de l’Inde, reste important. Cette bougeotte s’explique par l’investissement qu’ont certains membres impliqués dans l’association ETW-France qui entretient des liens étroits avec le Mataamritanandamayi Math au Kerala (Inde). La responsabilité personnelle de chacun se trouve engagée.

Laissons Yoann Gruson-Daniel conclure : « Il faut revoir profondément notre rapport à la mobilité, en mutualisant davantage, en réduisant la masse des véhicules, ainsi que la vitesse de déplacement [...]. Le message que je veux porter c’est qu’il est possible de trouver des alternatives à la voiture individuelle et lourde, même loin des grandes villes, en milieu rural, comme le sont souvent les écolieux. »

* Magnyéthique, L’Écohameau de Verfeil, la Bigotière, Le Moulin Bleu, la Ferme du Suchel, L’Écohameau du Plessis et Écoravie.
