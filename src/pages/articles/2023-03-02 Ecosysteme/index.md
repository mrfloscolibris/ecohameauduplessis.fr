---
title: L'écosystème de l'écohameau du Plessis'
date: "2023-03-02T22:40:32.169Z"
layout: post
type: blog
path: "/ecosysteme-2023/"
image: ./ecosysteme1.jpeg
categories:
  - présentation
---

A la création de l’écohameau du Plessis, alors qu’à l’époque aucune maison n’était sortie de terre, les membres de l’association ont rédigé la raison d’être du projet. Ce texte fondateur se conclut par ces mots : « L’écohameau s’intègre dans un ensemble plus large, qui comprend le Centre Amma, la maison seniors «les aînés du Plessis» et le village de Pontgouin. »

En ce début d’année 2023, il est intéressant de dresser un état des lieux et aussi faire le point sur les réalisations, parfois imprévues, qui existaient ou se sont développées dans le proche environnement de l'écohameau. Indéniablement, le village de Pontgouin bénéficie de la dynamique créée par l’installation d’une population attirée par ces projets.

![Ecosystème](./ecosysteme1.jpeg)

**Le Centre Amma - La Ferme du Plessis**

Le Centre Amma - La Ferme du Plessis est à l’origine de l’implantation de ces projets et de l'installation de nombreux habitants dans le voisinage. La vocation première du Centre reste celle d’un centre spirituel où sont mis en pratique les enseignements de Mata Amritanandamayi (Amma). D’autres activités y sont proposées, nombreuses et variées. Des stages couvrant un large éventail de disciplines sont ouverts à toutes et à tous. On vient parfois de loin pour y participer, s’initier à la permaculture, à l’apiculture, au yoga et autres disciplines.

![Ecosystème](./ecosysteme2.jpeg)

**Les jardins du Plessis**

Sur ces terres, les jardins du Plessis sont cultivés par deux maraîchers. Maguy et Thomas y produisent fruits et légumes labellises biologiques destinés à la vente locale. La culture a été facilitée depuis l'élévation d’une serre. Des membres de l'écohameau ont participé à ces travaux. Autour de la Ferme du Plessis, d’autres surfaces sont destinées a être cultivées en maraîchage biologique. Bientôt Loïc, qui habite à l'écohameau, s'installera aussi comme maraîcher bio.

![Ecosystème](./ecosysteme3.jpg)

**La maison des aînés**

Le terrain sur lequel commence à se dresser la maison des aînés du Plessis est situé entre la Ferme du Plessis et l’écohameau. Les travaux de VRD ont été menés par l'écohameau en 2019 et les travaux de construction du bâtiment ont commencé au tout début de l’année 2023 (__photo ci-dessus de la pose de la première pierre en présence des élus__). A terme, une vingtaine de studios accueilleront les résidents dans un cadre privilégié.

Les personnes prévoient d’y emménager tout début 2024. Clarisse, habitante de l'écohameau, fait partie de l'équipe de pilotage du projet et s'occupe notamment de la dimension sociale. Elle ne cache pas sa satisfaction de voir donnés les premiers coups de pelle : « __Ce bâtiment, pensé par ETW-France, accueillera des personnes, attirées par de nouveaux services, qui contribueront à la dynamique qu’on observe en ce moment.__ »

![Ecosystème](./ecosysteme4.jpeg)

**La Maison Jaune**

La Maison Jaune est un café-restaurant-épicerie associatif que dirige Claire, propriétaire d’une parcelle de l'écohameau. Cette maison, située au cœur du bourg, est devenue un lieu de rendez-vous et de rencontres ou se déroulent de multiples activités. Le restaurant offre en outre des menus végétariens et l’épicerie présente une gamme de  produits locaux et biologiques. L’esprit du lieu est convivial, participatif. « _Toutes sortes de gens viennent ici pour participer aux ateliers de musique, de poésie ou de poterie_ » explique Claire.
Récemment, les bénévoles de la Maison Jaune sont partenaires de l'association organisatrice de spectacles Pomprod. Ils ont accueilli plus de cent danseurs au dernier bal folk. L’expérience sera renouvellée et d’ores et déjà, on a sonné la mobilisation pour préparer la fête de la musique du mois de juin.

![Ecosystème](./ecosysteme5.jpg)

**L’école Montessori Colombes et Colibris**

L’école Colombes et Colibris a été mise en place à l’initiative d’Emmy et Loïc, habitants l’écohameau. Fanny, leur voisine, en est la présidente. Inspirée par la méthode Montessori, **cette école accueille pour l’instant 19 élèves de la maternelle au primaire**, un nombre qui devrait augmenter à la prochaine rentrée. L’ouverture de Colombes et Colibris a attiré sur la commune des jeunes couples, eux-mêmes à l’initiative de nouvelles associations. Citons « La Fabrik » qui propose des animations pour enfants dans la commune. « **Notre objectif est de pouvoir ouvrir une seconde classe**. » espèrent Emmy et Loïc qui planifient déjà un développement de l’école. Colombes et Colibris agit en partenariat avec d’autres associations locales comme Le Jeu de Peindre.

![Ecosystème](./ecosysteme6.jpeg)

**Un bénéfice certain pour la commune**

Les habitants de l'écohameau se sont investis dans ces structuress, portés par l’élan créateur qui les anime. Le dénominateur commun est l’enthousiasme qui caractérise cet engagement. A n’en pas douter, la commune bénéficie de ce mouvement.

La raison d’être de l'écohameau du Plessis est détaillée dans la page « Qui sommes nous ? » du site de l’ASL.
