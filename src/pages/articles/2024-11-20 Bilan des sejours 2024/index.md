---
title: Bilan des séjours et immersions 2024
date: "2024-11-20T22:40:32.169Z"
layout: post
type: blog
path: "/bilan-sejours-2024/"
image: ./sejour-2024-0.jpg
categories:
  - présentation
---

L’écohameau du Plessis propose depuis fin 2023 des séjours « découvertes » qui attirent des personnes de tous âges et de tous horizons curieuses de vivre l’expérience d’un mode de vie écologique et communautaire. En 2024, quatre séjours ont permis à une quarantaine de personnes – enfants compris - de partager cette immersion. De leur côté, les membres de l’écohameau ont acquis un savoir-faire qui leur permet de répondre aux attentes variées de leurs invités. La création d’un pôle « Maison commune », en charge notamment de la préparation des séjours, a été déterminante pour structurer l’accueil des visiteurs.
Membre de ce pôle, Emmy est responsable de ces séjours : « Notre but est de permettre aux personnes accueillies de découvrir notre mode de vie, notre organisation, et pourquoi pas, de leur donner envie de rejoindre un mode de vie participatif et écologique. Ces rencontres nous permettent d’échanger avec des personnes partageant nos valeurs et aspirations. Pour toucher les personnes intéressées, il est essentiel de communiquer largement. Notre organisation s’adapte et le programme devient progressivement plus riche et varié. »

![Séjours 2024](./sejour-2024-0.jpg) _Accueil de participants de la Pépinière Oasis pour une semaine-découverte en juillet 2024._

**Un programme diversifié**

L'accueil de groupes est désormais possible grâce à la mise en fonctionnement de la Maison Commune. Construite en grande partie par les membres de l’écohameau et des artisans locaux, ce bâtiment offre de nombreuses facilités.
Les séjours sont rythmés par une variété d’activités : chantiers, visites, rencontres, et sorties à vélo, offrant aux participants l’occasion de partager des moments avec les habitants, que ce soit lors des travaux en cours ou dans les jardins. Des réunions d’information théoriques sont également proposées, couvrant des sujets tels que la gouvernance, les aspects administratifs et financiers, ou la projection du film de J-Y Philippe « L’écohameau, un chemin d’aventure », qui illustre la vie quotidienne au Plessis.
Le programme s’organise alors généralement autour d’un chantier collectif le matin, d’un temps pédagogique (visite ou atelier théorique) l’après-midi et d’un repas partagé ou d’un temps d’échange sur un thème le soir.

![Séjours 2024](./sejour-2024-1.jpg) _Mathieu anime un atelier sur le montage juridique et financier du projet auprès des participants d'une semaine découverte._
![Séjours 2024](./sejour-2024-2.jpg) _Claudine fait visiter sa maison autoconstruite pendant la semaine découverte d'octobre 2024._

En plus de visiter les maisons de l’écohameau, les participants visitent des lieux locaux comme le [Centre Amma](https://www.etw-france.org/les-centres-en-france/la-ferme-du-plessis/), le café-épicerie associatif [« La Maison Jaune »](https://www.lamaisonjaunepontgouin.fr/), les serres de maraîchage biologique de [« l’éveil du champ »](https://www.leveilduchamp.com/), et l’école Montessori [« Colombes et Colibris »](https://www.ecole-colombes-et-colibris.com/). Certaines excursions mènent plus loin, comme à l’espace de coliving et coworking rural « La Mutinerie », à la production d’herbes aromatiques à Vaupillon ou à l’écolieu du [Moulin Bleu](https://lemoulinbleu.org/).

![Séjours 2024](./sejour-2024-3.jpg) _Loïc, habitant de l'écohameau, accueille les participants pour une activité de maraîchage bio sur sa ferme "l'Eveil du champ"._

**Bilan d’un séjour enrichissant**

Chaque semaine découverte se conclut par une réunion où les participants et habitants de l’écohameau partagent leurs impressions. Ces discussions enrichissent l’expérience des visiteurs et améliorent l’accueil des groupes. Les mots qui reviennent fréquemment dans le bilan des expériences vécues pendant ces périodes sont diversité, authenticité, simplicité et honnêteté.
Une des conséquences de ces bilans a été l’émergence de l’idée de créer un livret d’introduction détaillant les activités proposées. L'importance des échanges intergénérationnels a également été mise en avant, notamment lors des balades à vélo avec les enfants.

**Des profils très variés**

Les participants de ces séjours viennent en général avec l’intention de mieux connaître la vie en habitat participatif et si cela peut leur convenir. Certains envisagent mêe de candidater pour rejoindre l’écohameau du Plessis et le séjour est un passage obligé du processus d’intégration dans le projet. On peut citer Anthony qui a offert ce séjour à sa compagne Solène pour son anniversaire. C’était pour lui une vraie découverte et il a même partagé au final son métier de mangaka dans un atelier de dessin de mangas, créant un moment d’échange créatif et enrichissant. Ces séjours découverte ont plus généralement permis de tisser des liens entre les habitants et Florence, Benoit, Myriam, François, Sara, Natacha… qui pour beaucoup sont amenés à revenir de temps en temps. Les enfants de l’écohameau y trouvent aussi une belle opportunité pour se faire de nouveaux amis pour une semaine, surtout lors de moments de jeux dans le trampoline.

Le séjour de juillet était très rempli et réservé à des participants de la formation [« Pépinière Oasis »](https://cooperative-oasis.org/nos-offres/pepiniere-oasis/) de la Coopérative Oasis. Cette formation de 6 mois est un véritable incubateur pour des dizaines de porteurs de projets d’écolieux et chacun peut choisir 3 séjours parmi une douzaine de propositions. Certains avaient donc choisi l’écohameau, pour découvrir ce montage original et aussi souvent par la proximité avec le Centre Amma. D’autres membres de cette formation viennent d’ailleurs sur d’autres séjours, comme Valérie, fondatrice d’un cabinet de plaidoyer écologique, qui est venue passer 1 semaine pendant les vacances de la Toussaint avec deux de ses filles.

A noter que l’édition 2025 de la pépinière Oasis recommence en avril 2025. La semaine-découverte de juillet 2025 sera ainsi déjà pleine avec les porteurs de projet de cette formation qui auront choisi ce séjour.

![Séjours 2024](./sejour-2024-4.jpg) _France 3 a loué la maison commune pour une journée de tournage en septembre 2024 à l'écohameau._

**Un accueil en plein développement**

Au-delà de ces séjours organisés ([voir le planning 2025](https://ecohameauduplessis.fr/la-maison-commune)), la Maison commune est capable d’accueillir d’autres groupes pour des moments de travail et de formation. Parmi les séjours marquants des premiers mois depuis [l’inauguration en mai](https://www.ecohameauduplessis.fr/retours-inauguration-maison-commune/), la maison commune a accueilli la venue de l’équipe de FR3 en septembre en tournage pour une série sur les défis environnementaux et un séminaire annuel pour le groupe Les écologistes du Conseil Régional (qui a participé au financement ce la maison commune). L’accueil a même été international puisque pendant une semaine en septembre des représentants des réseaux d’écovillages belges et hollandais sont venus travailler sur place avec Mathieu, par ailleurs directeur de la Coopérative Oasis, pour produire un MOOC (formation en ligne) en flamand sur la création d’écovillages dans le cadre d’un projet européen à 3pays auquel la coopérative participe.

![Séjours 2024](./sejour-2024-5.jpg) _Les Belges et Hollandais présentent leur réseaux d'écovillages aux habitants de l'écohameau pendant leur séjour de création d'une formation en ligne._


Tous ces séjours mettent en avant les initiatives écologiques et le mode de vie collectif, permettant aux visiteurs d’apprendre, de partager et de contribuer activement à la vie du lieu. Ils permettent aussi au collectif d"assurer le remboursement du prêt contracté pour construire la maison commune.
L’ouverture prochaine de nouveaux espaces au deuxième étage devrait accroître la capacité d’accueil, offrant à l’écohameau de nouvelles perspectives pour l’avenir. Le bilan des 6 premiers mois de fonctionnement de la salle commune est en tout cas déjà très encourageant !

**Pour tout savoir sur les séjours decouvertes : [cliquez ici](https://ecohameauduplessis.fr/la-maison-commune/)**.
