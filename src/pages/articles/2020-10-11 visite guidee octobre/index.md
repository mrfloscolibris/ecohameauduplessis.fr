---
title: 50 visiteurs découvrent l’éco-hameau du Plessis
date: "2020-10-10T22:40:32.169Z"
layout: post
type: blog
path: "/visite-octobre-2020/"
image: ./visiteoctobre2.JPG
categories:
  - présentation
---

Les conditions étaient réunies pour assurer le succès de la troisième visite guidée organisée le 10 octobre 2020.
Le soleil et le public - une cinquantaine de personnes - étaient au rendez-vous, presque sans aucune communication.

L’équipe d’éco-batisseurs était prête à accueillir une cinquantaine de visiteurs qui s’étaient inscrits au préalable. Dès 14 heures, Mathieu Labonne et Pierre Tallandier présentaient l’éco-hameau dans son ensemble et expliquaient le déroulement de l’après-midi. D’emblée, forts de l'expérience des 2 premières visites guidées, les organisateurs avaient prévu la constitution de deux groupes, avant un temps en salle sur les sujets plus théoriques.

![Visite octobre 2020](./visiteoctobre1.JPG) _Mathieu Labonne a présenté le déroulement et l’organisation de la visite._

Le premier partait à la découverte du terrain pour comprendre l’organisation générale : le regroupement des maisons autour de placettes, le système de phyto-épuration, les espaces cultivés... Cela permettait d’avoir une vue générale sur ce projet.

![Visite octobre 2020](./visiteoctobre2.JPG) _Un groupe de visiteurs à la découverte des constructions et des jardins._

Pour l’anecdote, l’animation était assurée sur le terrain. Des membres du projet s’employaient en parallèle à nettoyer la mare des massettes qui l’envahissaient. Pendant l’opération, dans la bonne humeur, la chute de quelques jardiniers ayant perdu l’équilibre dans l’eau a amusé les visiteurs qui passaient.

![Visite octobre 2020](./visiteoctobre3bis.JPG)


![Visite octobre 2020](./visiteoctobre3.JPG) _Les travaux de nettoyage de la mare ont suscité intérêt et... rires._

L’autre groupe était orienté vers Abel Grugeon, auto-constructeur dans l’éco-hameau, qui présentait les différentes techniques de soubassement, d’élévation, d’isolation, etc. Autodidacte, ce menuisier de métier, a détaillé toutes les étapes de la construction en bois et paille, des fondations au toit végétal.

![Visite octobre 2020](./visiteoctobre4.JPG) _Chloé et Brieuc, de l’Espace Info Energie étaient venus présenter leur actiité et répondre aux questions du public._

Après les déambulations conduites dans le terrain par Lucile Glodt et la visite de la maison en construction de Claudine Matt, les deux groupes ont rejoint la ferme du Plessis où on leur a présenté les aspects juridiques et financiers. Cette intervention, qui pour certains pouvait paraître indigeste, a en fait
soulevé de nombreuses questions. L’assistance, curieuse, a pu comprendre qu’un tel projet ne peut se monter sans une organisation. Il faut être doté d’outils de gouvernance, de gestion et de communication adaptés. L’adhésion à une raison d’être commune apparaît être une condition incontournable dans les projets d’habitat partagé.

![Visite octobre 2020](./visiteoctobre5.JPG) _Pierre et Chantal, de Lucé (28), sont venus avec leur petit-fils Sacha. De passage dans un organisme d’amélioration de l’habitat, on leur avait conseillé de venir découvrir les maisons en bois-paille._

Les discussions plus informelles ont continué autour d’une tisane ou d’un café dans les locaux du Centre. Ce furent des moments d’échanges très intéressants, autant pour les visiteurs avides d’informations que pour les membres de l’éco-hameau ravis de partager leur enthousiasme.

![Visite octobre 2020](./visiteoctobre5bis.jpg) _Fabienne, Sylvie, Lise et Gillian ont apprécié la visite : « Cela permet de se remettre en question sur de nombreux sujets. »_

![Visite octobre 2020](./visiteoctobre6.JPG) _Henri et Corinne, venus de la région parisienne, avaient entendu parler de l’éco-hameau du Plessis par leurs enfants._

Ces échanges ont permis à ceux pour qui la construction d’une maison individuelle pourrait générer trop de soucis d’envisager une alternative. La résidence senior doit sortir de terre à proximité de l’éco-hameau dans quelques mois. Cette résidence offre aux retraités la possibilité d’occuper un studio dans cet immeuble construit avec des matériaux écologique. Cette solution a intéressé plusieurs sexagénaires qui ont pris les renseignements pour avoir plus de précisions sur ce projet.

![Visite octobre 2020](./visiteoctobre7.JPG) _Danny, de Viroflay, a montré beaucoup d’intérêt pour le projet de maison senior._

Rendez-vous est donné à tous ces visiteurs pour constater dans quelques mois ou années l’évolution des travaux. Ils sont impatients de voir la maison commune dont la première « botte de paille » doit être posée en 2021. La prochaine visite guidée est programmée pour le mois de mars 2021, après la période hivernale.
