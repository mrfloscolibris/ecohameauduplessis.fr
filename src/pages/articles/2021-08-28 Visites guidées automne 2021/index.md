---
title: Les visites guidées à l'automne 2021
date: "2021-08-24T22:40:32.169Z"
layout: post
type: blog
path: "/visites-guidees-automne-2021/"
image: ./visiteautomne2021.jpg
categories:
  - présentation
---

L'écohameau du Plessis se construit et de plus en plus de personnes veulent venir le visiter. Pour optimiser le temps passé par les membres du projet et regrouper les visiteurs, nous avons choisi de proposer des visites guidées à des dates précises. Elles sont en entrée libre et il est possible de faire un don à la fin. Sur d'autres créneaux, nous demanderons une participation financière en fonction du temps passé.

Voici les dates des dates des visites en entrée libre pour l'automne 2021 :
- le **dimanche 19 septembre** à 13h30
- le **dimanche 7 novembre** à 14h

Le 17 septembre, la visite s'inscrit dans les journées du Patrimoine et peut être couplé avec une visite guidée du Centre Amma- Ferme du Plessis, et notamment un spectacle à 15h30.

Ces visites durent 2 heures et permettent d'aborder l'histoire au projet, son organisation humaine, juridique et financière et les différentes techniques écologiques utilisées. Elles se terminent en général par un goûter au Centre Amma - Ferme du Plessis (situé à 200m) pour celles et ceux qui le souhaitent.

Pour s'inscrire il est demandé de renseigner ce [formulaire d'inscription](https://docs.google.com/forms/d/e/1FAIpQLSc-W5f882bCTGVao8CWA3elNz-d5TywcyCTVbHDyy_0c6qAqQ/viewform).

Il n'y aura pas de visites ensuite avant mars 2022 (pause hivernale).
