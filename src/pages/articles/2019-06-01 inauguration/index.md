---
title: L'inauguration du projet en mai 2019
date: "2019-06-01T22:40:32.169Z"
layout: post
type: blog
path: "/inauguration-mai2019/"
image: ./inauguration-plantation.JPG
categories:
  - présentation
---

_Le 5 mai 2019, l'écohameau du Plessis a été officiellement inauguré après de nombreuses années de travail et la construction des premières maisons. De nombreux élus du territoire étaient présents et des habitants des environs ont ainsi pu mieux comprendre les chantiers en cours._

![plantation](./discours-inauguration.JPG) _Après un discours de Mathieu Labonne, fondateur du projet, 3 élus ont pris la parole et partagé leur vision d'un tel projet._

Après plusieurs années de reflexion, de discussion avec les parties prenantes du territoire et finalement de chantier, les membres du projet avaient choisi le 5 mai 2019 pour inaugurer officiellement le projet, et ainsi inviter de nombreux acteurs et voisins.

Cet événement s'inscrit dans la forte volonté du projet d'être utile au territoire, de faciliter le développement du village de Pontgouin et de s'ancrer localement. Malgré le vent et le froid de ce matin de mai, une centaine de personnes était présente, dont une vingtaine de membres du projet, des habitants du village, des élus et quelques partenaires, notamment des entreprises du bâtiment du territoire.

L'inauguration a commencé par un temps de discours. Mathieu Labonne, fondateur de l'écohameau, a d'abord rappelé l'histoire du projet, les nombreuses étapes qu'il a fallu traverser et a remercié l'ensemble des personnes qui ont permis le projet. Réussir une telle initiative passe en effet par la coopération des acteurs institutionnels du territoire, d'entreprises.... mais aussi ici par le soutien apporté par le Centre Amma, Ferme du Plessis.
Le maire de Pontgouin, Jean-Claude Friesse, a ensuite rappelé son soutien au projet, auquel il croit depuis longtemps, et a notamment insisté sur l'importance de minimiser les surfaces bâties pour protéger les terres, une des intentions des membres de l'écohameau.
Charles Fournier, vice-président de la Région Centre Val de Loire, a alors pris la parole pour expliquer en quoi et pourquoi la région soutient ce genre d'initiatives et comment elle contribue à inventer de nouvelles façons d'habiter et de créer du lien social.
Enfin, Laure de la Raudière, députée, a félicité les membres pour leur tenacité et l'exemple qu'ils montraient avec cette réalisation. Les discours encourageants des élus ont touché les membres du projet qui sont ravis de voir l'accueil de plus en plus positif de leur initiative.

![plantation](./inauguration-plantation.JPG) _Un tilleul a été planté à l'entrée de l'écohameau pour honorer le lien entre l'écohameau et le village de Pontgouin._


Après les discours, une plantation collective d'un tilleul a eu lieu, animé par Devaraj et Shambo, membres du collectif. Enfants et élus ont chacun pu contribuer à mettre en terre cet arbre qui symbolisera le lien entre le village et l'écohameau.

Les participants de la matinée se sont ensuite répartis en 4 groupes. Des visites guidées ont permis de découvrir le fonctionnement de la phytoépuration, les techniques de construction des premières maisons, les aménagements paysagers... Ce fut l'occasion pour les habitants de Pontgouin de poser leurs questions et continuer à mieux comprendre les motivations des membres de l'écohameau.

L'inauguration s'est terminé par un repas végétarien sous forme de buffet, préparé par plusieurs membres de l'écohameau. Les échanges étaient conviviaux et chaleureux, malgré la météo peu clémente.
Cette inauguration est une étape importante dans ce long chemin qu'est la construction de l'écohameau du Plessis. La gratitude pour le chemin déjà parcouru était palpable et a nourri l'enthousiasme de chacun et chacune à aller plus loin.


Découvrez la vidéo de l'inauguration réalisée par Cécile Avril, membre du projet, [en cliquant ici](https://www.youtube.com/watch?v=6gaTXUjteCY&feature=youtu.be).
<iframe width="560" height="315" src="https://www.youtube.com/embed/6gaTXUjteCY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
