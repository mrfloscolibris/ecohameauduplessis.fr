---
title: Le pari de l'autoconstruction
date: "2023-03-08T22:40:32.169Z"
layout: post
type: blog
path: "/pari-autoconstruction/"
image: ./pari-autoconstruction1.jpeg
categories:
  - présentation
---

**Bien sûr, il s’agit d’un choix !**
A la base, l’idée de réaliser « la maison de ses rêves » en auto-construction, complète ou partielle, était installée dans l’esprit des propriétaires de l’éco-hameau.

Il n’est pas si lointain le temps où chacun pouvait se lancer dans un projet de construction avec un réalisme serein. Certes, se lancer dans un tel chantier demandait des moyens financiers. On les qualifierait aujourd’hui de raisonnables.

Depuis quelques mois, la donne a changé. Les devis ont explosé, obligeant à reconsidérer les projets initiaux. Gisèle s’explique : « __En quelques semaines, les fournisseurs ont augmenté leurs prix de telle manière que je dois revoir mon projet__ ». Certains retardent le dépôt de permis de construire. D’autres envisagent d’en faire plus par eux-même afin d’alléger le prix de leur habitation. Les dernières conditions imposées par l’augmentation du prix des matériaux ont renforcé le besoin de participer personnellement à la construction.
Déjà, auparavant, les premiers arrivants, les pionniers, ont eu recours à l’auto-construction.

![Autoconstruction](./pari-autoconstruction1.jpeg)

**De A à Z, un pari fou**

Pour Abel et Lucile, pour Vishwan, le choix était écrit à l’avance. Par leurs compétences dans les différents métiers, ils ont pu se lancer dans la réalisation de A à Z de leurs maisons. En respectant le cahier des charges, ils ont choisi techniques et matériaux respectueux de l’environnement.

Ainsi, dans un article paru sur le sujet dans le dossier Tour de France des Ecolieux, Abel décrit les techniques choisies. Il peut conclure après deux ans de travail intensif : « __Je n’ai pas fait le calcul, mais notre maison stocke peut-être plus de carbone qu’il n’en a fallu pour la construire. Et à l’usage, sans être passive, elle va peu consommer. »

**Formation, partage et entraide**

Pour d’autres, que la montée d’une structure aussi importante peut effrayer, l’auto-construction s’est limitée aux aménagements extérieurs et intérieurs. Cette démarche nécessite une formation, souvent acquise lors de longues soirées passées à consulter des tutoriels. Maël a ainsi si bien appris le montage de l’électricité que le consuel n’a rien trouvé à redire avant d’attribuer le quitus. Par contre, il lui a fallu refaire une chape trop maigre pour la pose du carrelage.

Loïc et Emmy ont eu eux aussi des surprises avec le système de filtration d’eau de pluie. « __On apprend de ses erreurs__ » explique Emmy avec philosophie. Pascal a passé du temps sur son toît pour déceler la cause des fuites en cas de précipitations abondantes. Le problème est résolu mais il avoue qu’il se serait bien passé de ce stress supplémentaire : « __Il y a tellement de choix à faire quotidiennement que toute contrariété prend de plus amples proportions__ » Pour tous, ils ont recours à l’entraide. Parfois, elle vient d’amis ou de membres de la famille.

![Autoconstruction](./pari-autoconstruction2.jpeg)
Didier et Vincent sont venus aider leurs fils à monter les cloisons ou poser le carrelage. Souvent, ce sont des habitants de l'éco-hameau qui viennent, ponctuellement ou à plus long terme, apporter leur soutien. On se donne ainsi des coups de main.

**Pour les bâtiments collectifs aussi**

Les principes de l’auto-construction ne s’appliquent pas seulement aux constructions individuelles. Les bâtiments collectifs ont été réalisés en grande partie par les habitants qui se réunissent à l’occasion de chantiers conviviaux. Sur la maison commune, seules les fondations et la couverture ont été confiés à des artisans locaux. Tout le reste, y compris électricité, VMC, etc, a été étudié et mis en œuvre par le pôle « travaux ».
Cela n’empêche pas de rencontrer les déboires. Luc se souvient du système de pompe pour la laverie qu’il a fallu réparer et adapter. On tire des leçons bénéfiques de ces déboires.

![Autoconstruction](./pari-autoconstruction3.jpeg)

**Une belle leçon d’optimisme**
Une des prochaines maisons qui va sortir de terre sera celle de Rémy. Pour lui, la question ne se pose pas : « __Vu mon budget, je vais devoir tout faire.__ » Rémy a déjà recruté oncle et paternel qui prévoient un séjour de plusieurs semaines. « __Je compte pour cela sur des membres de la famille et... sur mes voisins__ » ajoute-t’il
malicieusement.

Malgré une conjoncture défavorable, la construction d’un habitat écologique reste possible. Cela renforce l’idée d’auto-construction qui inspire les habitants de l’éco-hameau. Plus que de l’argent, c’est du temps et de l’amitié qui sont nécessaires pour réaliser de tels projets.

![Autoconstruction](./pari-autoconstruction4.jpeg)
