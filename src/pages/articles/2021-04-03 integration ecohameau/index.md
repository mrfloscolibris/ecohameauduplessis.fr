---
title: L'intégration dans l'écohameau, un processus vivant
date: "2021-04-03T22:40:32.169Z"
layout: post
type: blog
path: "/integration-ecohameau/"
image: ./integration1.JPG
categories:
  - présentation
---

L'écohameau est un organisme vivant qui se renouvelle sans cesse. Depuis les premiers échanges en 2013, la situation n’a cessé d’évoluer. La quasi totalité des parcelles ont été vendues dans un premier temps. Par la suite, les circonstances ont amené certains propriétaires à proposer leur terrain à la revente.
D’emblée, les conditions avaient été dictées pour éviter une surenchère, une course à la plus-value. Tirer profit de la vente d’une parcelle serait contraire a l’esprit qui anime la communauté.

Force est de constater qu’il y a des demandes. Ayant lu un article consacré à l’habitat participatif ou ayant participé à une visite organisée, des personnes se sont montrées intéressées. Le simple bouche à oreille a éveillé la curiosité de familles désireuses de changer de vie, de rejoindre une expérience humaine. Certains visiteurs de la Ferme du Plessis se sont sentis concernés.  On le constate, les motivations des éventuels acheteurs sont diverses et variées...

 ![Processus d'intégration](./integration1.JPG)

## De nombreuses candidatures

Par exemple, Stéphanie et Olivier de la région parisienne se sont présentés lors d'une réunion plénière :
_« Depuis environ 4 ans, nous sommes engagés dans une démarche de transition écologique et nous souhaitons continuer l’exploration en rejoignant une aventure collective de vivre ensemble et de connexion à la nature qui correspond à nos besoins de sens, de liens et de coopération. L’écohameau du Plessis est une aventure humaine dans laquelle nous nous projetons en famille pour vivre dans la sobriété et la joie, dans un esprit de gouvernance partagée.»_

Gisèle explique pour sa part l’intérêt de la proximité de la Ferme du Plessis et des activités liées à la nature : 
_« Le concept de l'écohameau m’attire particulièrement. En effet, je suis née dans la campagne gersoise et j’adore la nature, le jardinage, les arbres... J’ai d’ailleurs suivi plusieurs stages relatifs à la nature au centre Amma (jardinage, taille des arbres, forêt nourricière....).»_

Toutefois, les constructeurs déjà investis se posaient la question : comment accueillir au mieux ces postulants ?  Il fallait comprendre la démarche de ces candidats. On devait, pour certains, les placer face à la réalité des obstacles à surmonter quand on se lance dans une telle aventure humaine et constructive.

![Processus d'intégration](./integration3.JPG)

## Veiller à l’harmonie

Il convenait donc d’instaurer un processus d’accueil qui permette de recevoir chaque demande avec bienveillance et lucidité. Ainsi a été créé un groupe de travail qui, progressivement, a élaboré les étapes nécessaires à la bonne intégration de nouveaux arrivants.

Pierre, membre du « comité d’accueil, explique : _« Après un premier contact, je m’entretiens avec la personne intéressée pour bien présenter le projet et, ensemble, mieux cerner la candidature. Ensuite, une visite d’une journée est organisée pour découvrir les lieux et rencontrer deux ou trois membres du projet.»_

Si la candidature se confirme, le lien est fait avec le comité de pilotage (COPIL) en demandant une lettre de motivation, en fournissant les documents nécessaires à leur bonne compréhension du projet (raison d'être, plans, règlement de construction, etc.) et en les invitant à la prochaine réunion plénière.

Annie complète : _« On proposera ensuite un séjour à l'écohameau pour rencontrer les habitants, partager les travaux collectifs, se rendre compte des implications qu’entraîne un tel choix. Ces jours passés ensemble créent du lien, condition indispensable à la vie dans ce collectif. »_

![Processus d'intégration](./integration4.JPG)

C’est ainsi que Gisèle a pu, lors de son séjour, participer à une formation de greffe sur arbres fruitiers.
Les lettres de motivation sont toutes différentes et toutes sont riches.

Sandrine détaille dans sa lettre son itinéraire de la France à l’Afrique, du Québec à la Belgique avant de faire le choix de se fixer a Pontgouin : _« Pendant 2 ans j’ai petit à petit rencontré les habitants dont les maisons sortaient de terre, leur demandant conseils et avis sur leur projet. Aujourd’hui j’envisage de construire avec l’aide d’un architecte ou d’un maître d’œuvre. Je dispose des fonds nécessaires et la rencontre avec le collectif de l'écohameau me conforte dans ce choix.»_

Pour Rémy, qui habite depuis plusieurs mois dans le village, s’installer dans l’écohameau correspond à une démarche qui englobe l’environnement proche :
_« C'est aussi l'ouverture aux autres, voisins de l'écohameau, voisins de Pontgouin et aux voisins du monde. Contribuer au village et à des projets extérieurs est pour moi très important. J’ai entamé une collaboration avec la Mairie : j’utilise la salle des fêtes pour répéter un spectacle en création, spectacle qui sera joué devant les enfants de la commune en fin d’année scolaire. Je souhaite que ce premier échange donne des suites comme l'ouverture d'ateliers cirque dans la salle des fêtes, partenariat avec l’école de musique, interventions artistiques à l'école et à l'EPHAD que j’ai déjà contacté. Je vais également m'impliquer dans l'association « les Jardins du Plessis » ce printemps 2021. »_

![Processus d'intégration](./integration5.JPG)

## Un processus construit sur l’expérience

L’accueil d’éventuels nouveaux constructeurs demande du temps. Le processus peut paraître long et fastidieux. L’expérience montre qu’il est indispensable de respecter pas à pas ce cheminement. Il est la garantie d’une bonne compréhension du projet par le candidat avant un engagement important.
Pierre ajoute : _« Il est essentiel que le groupe connaisse la personne qu’il accueille. Cela a des conséquences importantes sur le long terme. On ne peut négliger ces étapes »_

Le « vivre ensemble » se construit chaque jour, dans un système qui évolue constamment.

![Processus d'intégration](./integration6.JPG)
