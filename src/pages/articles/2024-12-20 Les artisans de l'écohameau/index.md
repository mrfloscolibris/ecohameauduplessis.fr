---
title: A la rencontre des constructeurs et artisans de l'écohameau
date: "2024-12-20T22:40:32.169Z"
layout: post
type: blog
path: "/artisans-de-l-ecohameau/"
image: ./artisans-1.jpg
categories:
  - présentation
---

A fin 2024, 15 maisons individuelles écologiques et 2 bâtiments communs, dont la grande maison commune de 300 m², sont construits ou en cours de construction à l'écohameau. Même si elles partagent la même exigence écologique et un cahier des charges architectural commun, ces constructions sont toutes différentes. Dans des degrés divers, elles associent l'engagement des habitants sur leur propre chantier - et donc de l'autoconstruction - avec la participation d'artisans compétents locaux et amis du projet.

![Artisans](./artisans-1.jpg) _Abel, menuisier habitant l'écohameau, donne les indications pour un chantier participatif de la maison commune._

**Les premiers constructeurs de l'écohameau du Plessis : une aventure humaine et technique**

Le montage choisi pour l'Ecohameau du Plessis implique en effet que chacune des 28 parcelles soit construite par son premier propriétaire. Cette aventure que traverse chaque nouveau foyer est à la fois un parcours du combattant de plus de 18 mois, souvent semé d'embûche, mais constitue au fil du temps une vraie force et une expérience gratifiante qui contribue à la pérennité du projet et à son autonomie. En effet beaucoup des habitants se sont ainsi formés et ont développé des compétences pour les travaux ou simplement pour mener à bien un chantier avec des artisans.

Construire une maison demande en particulier de maîtriser beaucoup de démarches, que ce soit pour concevoir un plan de maison, engager les démarches administratives, ou se conformer aux normes, ou comme la règlementation environnementale 2020, le contrôle du consuel ou les tests d'étanchéité. Au fil du temps, les premiers habitants ont partagé leurs expériences, facilitant ainsi les démarches des nouveaux arrivants. Cette entraide a été cruciale pour le développement du projet et rend de plus accessible le fait de se lancer dans la construction d'une maison.

Pour soutenir cet ambitieux programme de travaux, un réseau d'artisans locaux s'est structuré au gré des nombreux chantiers qui ont eu lieu. Certains sont même habitants de l'écohameau. La plupart habitent dans le village de Pontgouin. Ils partagent à la fois des liens forts avec l'écohameau mais aussi des valeurs communes : un engagement pour des techniques écologiques et la volonté de voir ce projet aboutir. Ces artisans mettent leurs talents au service de ceux qui souhaitent construire, tout en s’adaptant aux budgets de chacun. Leur objectif est simple : offrir le meilleur service possible.

![Artisans](./artisans-2.jpg) _Abel, Fabien, Thomas et David, 4 des artisans professionnels qui travaillent très régulièrement sur les chantiers à l'écohameau._

**Des artisans polyvalents au service du projet**

Les compétences de ce réseau d'artisans couvrent un large éventail de travaux : de la charpente à la pose des cloisons, de l’électricité à la plomberie, en passant par la pose de VMC et de panneaux solaires. Abel Grugeon et Fabien Zorin, par exemple, sont respectivement menuisier et charpentier. Ils se sont au final associés pour réaliser la construction de maisons ensemble, en particulier avec des murs en bois-paille. Ils réalisent également les plans et se chargent des commandes de matériaux. "_J'apprécie la bonne entente dans notre équipe_", explique Fabien, soulignant l’esprit de collaboration. Ces deux artisans ont également construit la structure de la maison commune et viennent de terminer une maison individuelle avec toit végétalisé.
Actuellement, c’est David Steidel, de Purnhabitat, qui a pris le relai dans l’agencement intérieur de cette nouvelle maison. David est un artisan polyvalent, très sérieux, capable de répondre à des demandes variées et de réaliser tous les lots de second oeuvre. Il s'est installé dans la rue qui jouxte l'écohameau. "_C’est un plaisir de travailler ici, et je m’adapte à chaque projet_", confie-t-il.
Abel, de son côté, s’est formé à la pose de VMC (Ventilation Mécanique Contrôlée) et déclare : "_Je maîtrise désormais cette technique après avoir travaillé sur plusieurs chantiers dans l'écohameau._"
Thomas Vitrant, quant à lui, se consacre principalement à la menuiserie. "_C’est mon métier de base. J’aime les belles réalisations et les objets bien faits_", explique-t-il modestement. Les visiteurs sont souvent impressionnés par un escalier qu’il vient de réaliser, un véritable chef-d'œuvre de son savoir-faire. Il va prochainement réaliser quelques meubles en bois pour la maison commune.
Maxime Courtine intervient occasionnellement, en particulier pour les enduits et les peintures. "_C’est un travail de précision, et j’y prends toujours grand soin_", ajoute-t-il.

**Des compétences locales complémentaires au service du projet**

À ces artisans très proches de l'écohameau s’ajoutent des compétences internes qui se sont développés dans le projet et permettent de cultiver l'entraide.

Loïc, qui était ingénieur dans les énergies renouvelables avant de devenir maraîcher bio, apporte ses connaissances et ses conseils avisés sur les choix énergétiques. Vishwan, ayant passé sa vie à fréquenter les chantiers, partage également son expertise et ses idées. Il avait notammé formé Mathieu aux enduits terre qui a réalisé alors ceux de la maison commune. Claudine a aussi géré la construction de sa maison seule et a pu mettre à profit des années de participation à des chantiers participatifs. Maël, devenu électricien amateur après avoir construit sa maison, peut comprendre et intervenir sur les réseaux domestiques, offrant une approche pragmatique aux installations électriques. C'est lui qui a notamment réalisé toute l'électricité de la maison commune bénévolement.

Ainsi, toutes ces compétences permettront de rassurer les personnes qui se lanceront dans l’aventure de la construction de leur maison dans les à venir. Les maisons, dans leur conception et leur réalisation, peuvent ainsi respecter le cahier des charges qui vise à maîtriser l'empreinte écologique du projet. Toutes ces ressources humaines travaillent chaque jour dans cet esprit, soucieuse de bâtir durablement et respectueusement avec l’environnement. Ce maillage d'expériences concrètes et de compétences professionnelles est assurément devenu une des forces de l'écohameau au fil des années.

![Artisans](./artisans-3.jpg) _Abel, Maël, Mathieu, Mariette et Vishwan, lors de différents chantiers bénévoles._
