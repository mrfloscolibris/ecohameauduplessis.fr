---
title: Sortie du film "l'écohameau, un chemin d'aventure"
date: "2023-09-01T22:40:32.169Z"
layout: post
type: blog
path: "/film-chemin-aventure/"
image: ./film-ecohameau-home.jpg
categories:
  - présentation
---

En 2022, Jean-Yves Philippe a filmé l'écohameau pendant plusieurs mois.

Son documentaire, **l’écohameau, un chemin d’aventure**, est sorti en 2023 et est maintenant disponible à la location VOD ou à l'achat en DVD. Il présente l'écohameau en 2022 et donne une vue complète du fonctionnement du collectif mais aussi de l'écosystème qui entoure désormais l'écohameau.

![Affiche film](./film-ecohameau-page.jpg)

Voici 2 extraits du film à visionner :

<iframe src="https://player.vimeo.com/video/865536111?h=83912f8eda" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
<p><a href="https://vimeo.com/865536111">L&rsquo;&eacute;cohameau, un chemin d&rsquo;aventure (extrait 1)</a> from <a href="https://vimeo.com/filmsdunjour">Les Films d&#039;un Jour</a> on <a href="https://vimeo.com">Vimeo</a>.</p>

<iframe src="https://player.vimeo.com/video/865536235?h=89f22d585b" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
<p><a href="https://vimeo.com/865536235">L&rsquo;&eacute;cohameau, un chemin d&rsquo;aventure (extrait 2)</a> from <a href="https://vimeo.com/filmsdunjour">Les Films d&#039;un Jour</a> on <a href="https://vimeo.com">Vimeo</a>.</p>

Vous pouvez le commander en DVD sur le site suivant de l'Harmattan :
https://www.editions-harmattan.fr/index.asp?navig=catalogue&obj=video&no=175451
