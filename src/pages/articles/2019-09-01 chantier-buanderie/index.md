---
title: Un chantier participatif pour la première buanderie collective
date: "2019-09-01T22:40:32.169Z"
layout: post
type: blog
path: "/chantier-buanderie/"
image: ./buanderie-1.JPG
categories:
  - présentation
---

_Après le chantier de la phytoépuration qui a eu lieu à l'été 2018, le premier bâtiment commun est une buanderie collective. Le chantier a été commencé en juin 2019 et devrait se terminer fin 2019. Ainsi les premières familles pourront mutualiser des lave-linge et sèche-linge et ne pas encombrer leurs logements._

![Buanderie-abel](./buanderie-6.JPG) _Abel Grugeon, membre de l'écohameau et menuisier, a coordonné le chantier._

![Buanderie-paille](./buanderie-4.JPG) _Les ballots de paille ont été montés pendant la semaine de formation Experience Week organisée au Centre Amma et des membres du projet ont finalisé les enduits extérieurs de corps dans les semaines qui ont suivi._

![Buanderie-paille2](./buanderie-1.JPG) _Une bénévole du chantier de construction des murs en paille satisfaite du travail réalisé !_

![Buanderie-enduits1](./buanderie-3.JPG) _Enduits terre et chaux pour l'extérieur réalisés en septembre par une dizaine de membres du projet._

![Buanderie-enduits2](./buanderie-2.JPG) _L'extérieur a été terminé en octobre et il reste désormais à aménager l'intérieur de la buanderie._
