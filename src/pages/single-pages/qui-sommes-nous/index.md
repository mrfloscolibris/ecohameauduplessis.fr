---
title: "Qui sommes-nous ?"
date: "2018-01-29T22:12:03.284Z"
layout: page
path: "/qui-sommes-nous/"
---

## L'histoire du projet
**A l'origine du projet, on trouve le Centre Amma - Ferme du Plessis**. Depuis 2002, cette grande ferme fortifiée du
Moyen Âge est devenu un centre spirituel, fondé et inspiré par Amma, une grande figure spirituelle et humanitaire indienne. Depuis ses débuts, de nombreuses personnes sont venues vivre dans ses environs pour participer au fonctionnement du lieu et aux activités proposées.
Plusieurs de ces « voisins » ont partagé leur aspiration à un mode de vie en lien avec leurs valeurs (écologie, partage d’équipements, économie locale, solidarité...). Certains se sont fortement impliqués dans le développement des jardins de la Ferme du Plessis et dans la transformation de ce lieu en écosite pédagogique, utilisant les techniques de permaculture.

En 2014, un groupe, initialement d’une petite dizaine de personnes, s'est ainsi réuni régulièrement pour réfléchir à la création d’un écohameau sur une parcelle qui se situe à côté de la Ferme du Plessis. Après plusieurs mois de réunion, ces premiers participants du projet se sont accordés sur la raison d’être suivante :
"L’écohameau du Plessis est le **lieu de vie de plusieurs familles qui souhaitent incarner un mode de vie écologique et harmonieux**. Il soutient leur intention de partage, de sobriété, de simplicité, d’autonomie, et de respect de la Nature et des cycles de la Vie.
Pour réussir cette intention, les habitants cherchent à cultiver des valeurs de bienveillance et de responsabilité, afin de prendre soin du lieu, de la relation humaine, et de le rendre vivant. Ils visent **un équilibre entre l’intimité et la souveraineté individuelle d’une part, et une vie collective qui facilite la réussite des intentions du projet**.
L’écohameau s’intègre dans un ensemble plus large, qui comprend le Centre Amma, la maison séniors «les aînés du Plessis» et le village de Pontgouin. Dans ses choix de développement, il est attentif en particulier à prendre en compte et respecter la raison d’être du Centre Amma et ses activités."

![ferme du plessis](./img/bandeauqui1.jpg)

## Le collectif

Le projet peut accueillir un total de 28 familles. 26 foyers étaient déjà engagés mi 2019.
Les profils des participants sont très divers, ce qui traduit le souhait d’un projet de mixité sociale et générationnelle. 1/3 des propriétaires ont entre 20 et 40 ans, 1/3 entre 40 et 60 ans et 1/3 aura plus de 60 ans.
Au moment du lancement, le projet accueille une dizaine de jeunes enfants et plusieurs personnes habitant seules.
Les habitants sont originaires pour 1/3 des environs proches de l’écohameau (Pontgouin, Courville...), pour 1/3 de
région parisienne et 1/3 d’autres horizons.

Un **processus d’inclusion** permet aux membres du projet de choisir les futurs voisins, sur la base de rendez-vous et d’une lettre de motivation.

Les plénières régulières et l’engagement de certains membres dans des projets associatifs communs, notamment à la Ferme du Plessis, ont créé un **collectif solidaire et convivial**. La force du collectif tient à ce cheminement et l'expérience concrète d'avoir été de longue date impliqué dans la gestion de la Ferme du Plessis.

![le collectif](./img/bandeauqui2.jpg)


## L'écosystème du projet
L'écohameau du Plessis s'inscrit dans un ensemble de projets, inspirés par Amma, qui crée une véritable "archipel" de projets autour du [Centre Amma - Ferme du Plessis](https://www.etw-france.org/les-centres-en-france/la-ferme-du-plessis/). Aux côtés du Centre Amma et de l'écohameau, on trouve notamment :
- les **[Aînés du Plessis](https://www.ainesduplessis.fr/)**. Sensibles à la question du bien vieillir, l'association et le fonds de dotation ETW-France ont fondé une SCIC pour créer une résidence pour les séniors. Elle accueillira aussi bien des personnes seules que des couples au sein de 22 logements, dans un lieu de vie à dimension humaine, écologique et pensé collectivement. Le projet met l’accent sur la préservation et le développement du lien social, entre les résidents, avec la famille, mais aussi en favorisant l’ouverture de la résidence vers l’extérieur.
- les **Jardins du Plessis** est un projet de maraîchage biologique sur 1 hectare porté par un couple. Ils cuitvent diverses variétés de légumes de saison, petits fruits et aromatiques en agriculture biologique sur un terrain mis à disposition pour la Ferme du Plessis. A l'avenir d'autres agriculteurs pourraient s'installer une parcelle concomitante à celle de l'écohameau.

![les partenaires](./img/bandeauqui3.jpg)

Outre cet écosystème très proche, l'écohameau du Plessis tisse des liens avec de nombreux acteurs, institutions, citoyens ou associations, du territoire.

-----

Pour nous écrire, rendez-vous sur la [page contact](../contact).
