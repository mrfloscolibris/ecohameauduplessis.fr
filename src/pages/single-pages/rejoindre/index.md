---
title: "Rejoignez l'écohameau du Plessis"
date: "2019-01-29T22:12:03.284Z"
layout: page
path: "/rejoindre/"
---

_L’Ecohameau du Plessis en Eure-et-Loir cherche des personnes pour intégrer le projet en tant que futurs habitants. Lancé en 2013, l'écohameau est un projet collectif d’habitat participatif et écologique, regroupant à terme 28 foyers. Il se situe à Pontgouin en Eure-et-Loir, et est constitué de parcelles individuelles sur un terrain collectif, géré par les habitants via une association syndicale libre. A ce jour 13 foyers vivent sur place, une buanderie partagée est en service, et la maison commune de 250m2 vient d'être construite et permet désormais d’accueillir des activités et des visiteurs._

_Candidatez dès que possible pour les dernières places dans l'écohameau._

![rejoindre](./img/bandeau-rejoindre1.jpg)

## Description du projet

**Historique du projet et description du lieu**

Situé à Pontgouin en Eure-et-Loir (28), à quelques kilomètres de Chartres et à 1h30 de Paris, l'écohameau du Plessis est un habitat participatif écologique qui rassemblera 28 foyers. Initié en 2013, l'écohameau se construit depuis 2018. Il s’agit d’un ensemble de maisons indépendantes sur un terrain commun de 4,3 hectares. Pour permettre à chaque famille de gérer la construction de sa maison en tenant compte de son budget et de ses goûts, chaque foyer possède et construit sa maison, et plus des 4/5ème de la surface du terrain reste collective. La mutualisation grâce à des buanderies collectives ou des chambres d’amis dans la maison commune permet de réduire l’empreinte écologique des habitants.

**Raison d’être**

Notre vocation est d'instaurer un équilibre entre intimité et souveraineté individuelle d'une part, et vie collective, partagée, d’autre part. Nous cultivons des valeurs de bienveillance et de responsabilité afin de prendre soin du lieu et de la relation humaine.
Notre écohameau s'intègre dans un écosystème comprenant la Ferme du Plessis - Centre Amma, la maison séniors d'ETW-France et le village de Pontgouin avec la création d'une épicerie/café associatif et d'une école Montessori.

**Aspects spirituel**

L’écohameau est né d’une volonté de plusieurs habitants d'intégrer une dimension écologique et spirituelle à leur vie quotidienne. La spiritualité tient donc une place importante dans le projet, notamment les valeurs universelles enseignées / véhiculées par Amma, figure spirituelle et humanitaire indienne, et est ouvert à tout chemin spirituel, humaniste, laïc. Notre écohameau comprend ainsi des personnes, des familles, d’horizons et de traditions diverses.

**Description du collectif**

A l’heure actuelle 20 adultes et 10 enfants vivent déjà sur place et plusieurs autres foyers sont membres du projet mais n’ont pas encore construit leur maison. Nous travaillons dans des domaines très variés, tels que le maraîchage, l’éducation, la construction, le conseil, l’associatif, la santé. Le projet compte également plusieurs personnes retraitées. Chacun amène ainsi une expérience et des savoirs très diversifiés, et crée une grande richesse dans l’écohameau !

**Ce qui nous rassemble dans le concret**

Nous nous retrouvons dans l’action et le concret, notamment au travers des différents chantiers, des travaux, la gestion des jardins potagers et des espaces communs. Mais également selon les goûts autour de la musique, de l’apiculture, de la danse…
Les membres de l’écohameau sont également impliqués au Centre Amma et dans la vie du village aux alentours: une école Montessori, un café associatif, du maraîchage avec vente en direct...
D’autres commerces sont à proximité, ainsi que des écoles et collèges publics à 6km. L’écohameau est également proche d’une gare TER desservant Paris en 1h30 (ligne Paris-Le Mans).

![rejoindre](./img/bandeau-rejoindre2.jpg)

## Qui cherchons-nous?
Nous recherchons des personnes seules ou des familles de tous âges et profils pour nous rejoindre comme nouveaux habitants, avec :
 - Une volonté forte de s’engager concrètement et de participer activement, à la création de notre projet collectif sur un lieu très actif (projets au sein et autour de l’écohameau)
 - Une motivation certaine pour la vie collective et les relations inter-personnelles
 - Un intérêt pour la spiritualité ou être soi-même dans une démarche spirituelle
 - Une capacité financière pour mener à bien son projet de construction


## Processus de candidature
 - Après avoir réfléchi à votre désir d’intégrer notre écohameau, [envoyez nous un email avec vos motivations](/contact/), décrivez vous et indiquez vos coordonnées
 - Nous vous recontacterons pour discuter par téléphone pour un premier contact
 - Participez à une “semaine-découverte” pour découvrir le lieu, nous rencontrer et appréhender notre fonctionnement ([lien d’inscription](https://ecohameauduplessis.fr/la-maison-commune/))
 - Confirmez votre candidature et faites plusieurs choix de parcelles
 - Votre candidature est validée
 - Achat et... en route pour l'aventure !

<br />

## Ressources
 - [Introduction d'un film documentaire sur l’écohameau](https://vimeo.com/865536111)
 - [Présentation de l'écohameau dans la série Cap sur les Oasis](https://www.youtube.com/watch?v=6_lvYIiKMuc)
