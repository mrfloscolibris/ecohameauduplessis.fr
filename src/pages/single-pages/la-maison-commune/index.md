---
title: "La maison commune"
date: "2023-08-17T22:12:03.284Z"
layout: page
path: "/la-maison-commune/"
---

De 2022 à 2024, les habitants de l'écohameau ont construit la **maison commune**, une grande maison en bois, paille et terre, au centre du hameau, qui va devenir progressivement le coeur de la vie collective du projet et permettre d'accueillir des visiteurs. Découvrez ci-dessous :
**- une description de cette maison**
**- les séjours découvertes de l'écohameau**
**- les possibilités de location**

![maison commune](./img/bandeau-mc0.jpg)

## Qu'est-ce que la maison commune ?

Cette maison de plus de 250 m² habitables comprend :
- au rez-de-chaussée, une grand **salle polyvalente de 80 m²** pour des réunions, des ateliers, des fêtes, des formations... ainsi qu'une des 3 buanderies collectives.
- au premier étage, **cinq chambres d'amis**, des salles de bain etc une cuisine partagée
- au deuxième étage, **2 salles qui servent de coworking en semaine** et pour des activités entre habitants les soirs et week-ends
- un **atelier collectif** de 30 m² environ

Elle a été construite par des artisans locaux, dont Abel, menuisier habitant de l'écohameau, et par les habitants de l'écohameau, notamment lors de chantiers participatifs.

![maison commune](./img/bandeau-mc2.jpg)

## Les séjours-découvertes à l'écohameau du Plessis
Seul, en couple ou en famille, venez passer une semaine de vacances à l'Ecohameau du Plessis et découvrir le mode de vie en écolieu ! L'Écohameau du Plessis propose en effet les 5 chambres d’amis des habitants à la location pendant 2, 4 ou 6 jours.

**Le programme des semaines-découverte**

Pour les programmes de 6 jours, nous vous demandons d'arriver le dimanche en début d'après-midi.

Du lundi au samedi, chaque journée comprend :
- un chantier collectif de 10h à midi (jardins, écoconstruction)
- un atelier pédagogique, un temps sur le vivre ensemble, ou une visite dans l’après-midi : visite de l’écohameau, des maisons écologiques, du Centre Amma, de l’école Montessori et d’un autre écolieu du territoire, présentations des modèles juridiques et financiers de plusieurs écolieux…
- une soirée collective ou libre : dîners avec des habitants, bal folk, soirées jeux, projections de films...

Les activités sont encouragées mais restent optionnelles. Des informations sont mises à disposition sur les activités touristiques à faire dans la région, notamment dans le PNR du Perche.

Pour les repas, plusieurs options s’offrent à vous : cuisiner ensemble des repas dans la cuisine collective de la maison commune ou déjeuner à l’extérieur (La maison Jaune à Pontgouin, est un restaurant/café associatif et propose de bons repas de produits bios locaux). Vous serez invités chez l’habitant pour certains repas (type auberge espagnole : chacun apporte quelque chose à partager).

Vous pouvez sur ce lien [télécharger un modèle de programme de la formule sur 6 jours](./Programme-de-la-semaine-decouverte.pdf). Attention, ce programme est à titre indicatif. Nous faisons un programme spécifique à chaque semaine et ce déroulé peut évoluer en fonction des personnes qui animent la semaine.

Les programmes de 2 et 4 reprennent le même principe. Le séjour de 2 jours inclut par contre les repas (pension complète).

**Les tarifs**

Le séjour comprend l’hébergement dans la maison commune (belles chambres avec enduits en terre, cuisine collective et salle-de-bains partagées), les animations et temps collectifs.
Pour les séjours de 2 jours, le tarif inclut aussi les repas. pour les séjours de 4 ou 6 jours, un panier de légumes bio du village et de produits locaux vous est offert à l’arrivée. Il aura besoin d’être complété par vos soins.

Pour 6 jours, le tarif est de :
- 320€ pour une personne seule (petite chambre)
- 400€ pour un couple (petite chambre)
- 440€ pour une famille (couple avec 1 ou 2 enfants)

Découvrez les autres tarifs dans le formulaire plus bas.

**Les dates en 2025**
- 13 au 19 avril 2025 (6 jours)
- 30 avril - 4 mai 2025 (4 jours)
- 29 mai - 1er juin 2025 (4 jours)
- 13 au 19 juillet 2025 (6 jours)
- 19-21 septembre 2025 (WE)
- 19 au 25 octobre 2025 (6 jours)

Vous pourrez demander le programme détaillé de chacun de ces séjours environ 1 mois avant le séjour.

**Inscription**

Nous vous demandons d'abord de remplir [un formulaire de pré-inscription](https://docs.google.com/forms/d/e/1FAIpQLSfH6ZW4jioUAC9x9AjaOdvNuw1hiZb--Ax2dHbXjb5dYls_0A/viewform).
Vous serez ensuite recontactés rapidement pour vous donner les modalités de paiement (par virement en amont de votre venue).

![maison commune](./img/bandeau-mc3.jpg)

## Les locations hors séjour-découverte

En plus des séjours-découvertes il est possible de louer les chambres d'amis et la grande salle d'activités pour vos événements :
 - l'ensemble des 5 chambres et de la grande salle à 650 euros le week-end (2 nuits)
  - la grande salle sur une base de 150 euros par jour
   - une chambre sur la base de 45 à 55 euros la nuitée.

En parallèle, le coworking, situé au 2ème étage, ouvrira fin 2024.

[Pour venir séjourner en dehors des semaines découvertes ou louer la grande salle commune, contactez-nous](/contact/).
