---
title: "Le projet"
date: "2018-01-29T22:12:03.284Z"
layout: page
path: "/le-projet/"
---

## Pourquoi un écohameau ?
Face aux nombreux enjeux écologiques et sociaux, des collectifs citoyens créent de nouveau lieux écologiques et solidaires : écohameaux, habitats participatifs écologiques, tiers-lieux… Ces nouveaux lieux de vie inventent le monde de demain et l'écohameau du Plessis s'inscrit comme l'une de ces expérimentations. Il participe notamment au réseau des oasis, animé par la Coopérative Oasis.

L'écohameau propose à ses habitants des moyens de vivre de façon plus résiliente, sobre et autonome grâce à la puissance du collectif. Par la mutualisation, l'entraide et le partage, il devient en effet plus simple de consommer moins, de construire moins, de produire une partie de sa nourriture, mais aussi de s'organiser collectivement pour plus d'autonomie. Maisons écologiques, phytoépuration, toilettes sèches, bâtiments communs... toutes ces réalisations sont le fruit de la coopération entre les membres du projet.

Le collectif a choisi la forme d'un écohameau, c'est-à-dire d'un ensemble de maisons indépendantes, du fait de la configuration du terrain et de la taille du groupe. Des formats de propriété collective peuvent en effet être plus difficiles à gérer à aussi grande échelle. Nous avons eu la chance d'acquérir un terrain constructible mais non bâti à 200 m à peine de la Ferme du Plessis. Pour garder une forme d'intimité et par volonté de limiter les coûts, chaque famille possède et construit sa maison, mais plus des 4/5ème de la surface du terrain reste collective.

![vueexterieures](./img/bandeauprojet1.jpg)

## Le montage juridique et financier
Le projet est porté par une **association syndicale libre ou ASL**, qui sert notamment à gérer les biens communs. Ce fonctionnement est plus simple que celui lié à des propriétés collectives (SCI, coopératives d’habitants...) et
plus souple qu’une copropriété classique.

Chaque foyer possède une petite parcelle privée, soumise à un **règlement de construction qui contraint les choix individuels** : toilettes sèches obligatoires, matériaux biosourcés, respects de l'architecture locale, exigence thermique... Ce choix permet donc d'assurer une haute qualité écologique, tout en laissant chacun responsable des travaux sur sa maison. Cela facilite la diversité des projets (autoconstruction ou non, modes constructifs différents...).

L’ASL gère les parties collectives, qui représentent la majorité des surfaces. Cette structure possède
des statuts et un règlement intérieur.

Comme la majorité des habitants était identifiée au moment du dépôt du permis d'ménager, nous avons choisi de ne pas créer de structure intermédiaire, pour éviter une double mutation et éviter de constituer une structure à la durée de vie limitée. 26 foyers ont cosigné un permis d’aménager en 2017 et ont alors juridiquement été co-aménageurs. Ainsi chaque foyer a acheté directement sa parcelle et l’ASL constituée suite à l’acceptation du permis d’aménager possède le reste du terrain.

[**Télécharger les statuts de l'ASL**](./Statuts_signes.pdf).
[**Télécharger le règlement de construction**](./reglement_de_construction.pdf).


![plangeneral](./img/bandeauprojet2.jpg)
_crédits : [atelier108architecture](https://atelier108architecture.com/)_

## La dimension écologique et les plans du projet
Les habitants de l’écohameau sont très attachés à la dimension écologique de leur projet et à une recherche d’autonomie, avant tout alimentaire, mais aussi énergétique et en eau.

Les maisons suivent toute un règlement de construction qui assure que ce sont des maisons en matériaux biosourcés, quasi passives, chauffées sans énergie fossile, avec toilettes sèches, eau chaude solaire ou chauffe-eau thermo-dynamique... Chaque maison trouve ses propres solutions pour suivre ces contraintes et le projet constitue ainsi un **véritable laboratoire de l'éco-construction**.

Le collectif a fait le choix d'un assainissement écologique en installant **3 phytoépurations** avec Aquatiris. Ainsi les eaux grises sont recyclées et réintègrent le cycle naturel de l'eau. Cela participe au design global de la question hydrique, avec des noues, mares, bassins... car le climat local est humide en hiver mais très sec l'été. L'écosystème peut ainsi bénéficier de ces aménagements pour traverser les canicules estivales.

Le plan général des 4,3 hectares a été réalisé grâce à un **design en permaculture**, pour faciliter l'entretien. Plus de 100 arbres ont déjà été plantés, dont une majorité de fruitiers. Une forêt nourricière a été implantée. Ce design permet d'aller vers la production progressive d'une partie de la nourriture des habitants, avec des potagers collectifs au coeur des habitations.

Enfin, la **mutualisation grâce à des bâtiments communs** permet de réduire l'empreinte écologique des habitants. Ainsi des buanderies collectives ou des chambres d'amis dans la grande maison commune permettent de construire des maisons moins grandes, sans machine à laver.

![plangeneral](./img/bandeauprojet3.jpg)

## La gouvernance et le fonctionnement humain
Aujourd'hui l'ASL est administré par un **comité de pilotage constitué de 7 personnes élues suivant le principe de "l'élection sans candidat"**. En parallèle, des groupes de travail sont créés en fonction des besoins.
Les grandes décisions restent cependant du périmètre de la plénière, pour que chacun puisse y participer.

Les décisions se prennent au consentement, suivant les principes de la sociocratie. Une grande partie des membres expérimentait déjà la gouvernance participative existante à la Ferme du Plessis depuis 2011.

Avec l'emménagement progressif des familles, la gouvernance va encore évoluer pour confier un périmètre croissant de responsabilité à des pôles (bâtiments communs, jardins...).
