---
title: "Participer"
date: "2019-01-29T22:12:03.284Z"
layout: page
path: "/participer/"
---


## Rejoindre les habitants de l'écohameau
Le projet est encore en phase de construction et **cherche encore quelques familles**.
Afin d'assurer une pyramide des âges équilibrée, **nous privilégions des familles avec jeunes enfants ou des jeunes couples**.
Vous pouvez nous contacter puis venir nous rencontrer sur place, par exemple lors d'une plénière des habitants.
Le projet étant historiquement relié au Centre Amma - Ferme du Plessis, nous vous encourageons à découvrir ce lieu qui est ouvert toute l'année.
Lors de la première rencontre, nous pourrons vous présenter le processus d'engagement, qui permet d'intégrer potentiellement le collectif.<br /><br />
[Contactez-nous pour en savoir plus](/contact/).

![rejoindre](./img/bandeau-rejoindre.jpg)

## Participer à la construction écologique
Le projet se réalise principalement en autoconstruction et uniquement avec des techniques écologiques. **Il y a donc régulièrement des chantiers sur lesquels venir apprendre et participer**.<br />
Si vous souhaitez venir participer à un chantier sur place, contactez-nous pour savoir les chantiers en cours.
Nous ne prenons pas en charge l'hébergement et les repas. Il est possible de loger au Centre Amma - Ferme du Plessis à 200 mètres de l'écohameau.

![chantiers](./img/bandeau-chantier.jpg)

## Nous soutenir financièrement
L'association syndicale libre qui gère le lieu est à la recherche de financements pour les équipements collectifs du projet et pour pouvoir mettre en bien commun un maximum de son expérience.
Vous pouvez nous **envoyer un chèque de soutien à l'ordre de ASL Ecohameau du Plessis**, à l'adresse Ecohameau du Plessis, rue Paul Minard, 28190 Pontgouin.<br />
Merci d'avance pour votre générosité, au service d'un projet innovant qui expérimente un mode de vie écologique et solidaire !
