---
title: "Nous contacter"
date: "2018-01-29T22:12:03.284Z"
layout: page
path: "/contact/"
---
Si vous souhaitez nous contacter pour la location de chambres ou de salles dans [la maison commune de l'écohameau](https://www.ecohameauduplessis.fr/la-maison-commune/), merci de contacter **maisoncommune @ ecohameauduplessis.fr** _(recopier l'email en retirant les espaces)_.


Si vous souhaitez nous contacter pour les formules de [séjours-découverte](https://www.ecohameauduplessis.fr/la-maison-commune/), merci de contacter **sejourdecouverte @ ecohameauduplessis.fr**.


Pour toute autre question concernant l'écohameau, merci de contacter **contact @ ecohameauduplessis.fr**.
