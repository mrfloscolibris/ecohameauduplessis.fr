---
title: "Mentions légales"
date: "2018-01-29T22:12:03.284Z"
layout: page
path: "/mentions-legales/"
---

## Éditeur

Ecohameau du Plessis  
Association syndicale libre

Représentant légal : Mathieu Labonne

### Siège social

ASL Ecohameau du Plessis,
rue Paul Minard,
28190 Pontouin

Pour nous contacter : [utilisez notre formulaire de contact](/contact/)  

## Hébergeur

Gandi.net
63, 65 Boulevard Massena
75013 Paris  
RCS Paris B. 423 093 459  

## Informatique et libertés

Les informations recueillies sur ce site ou ses sous-domaines font l’objet d’un traitement informatique destinés aux besoins des services proposés par la Coopérative Oasis ou pour des traitements statistiques. En application des articles 39 et suivants de la loi du 6 janvier 1978 modifiée, vous bénéficiez d’un droit d’accès et de rectification aux informations qui vous concernent.  

Si vous souhaitez exercer ce droit et obtenir communication des informations vous concernant, veuillez vous adresser à : contact (at) cooperative-oasis (point) org  
Les contenus du site cooperative-oasis.org sont, en dehors des photos, soumis à la licence **Creative Commons BY-SA**.  
Pour améliorer l'accès à la culture et à la connaissance libres, nous autorisons de redistribuer ou réutiliser librement les contenus pour n'importe quel usage, y compris à des fins commerciales. Une telle utilisation n'est autorisée que si l'auteur est précisé, et la liberté de réutilisation et de redistribution s'applique également à tout travail dérivé de ces contributions.  

Certaines photos ont été fournies gracieusement par Patrick Lazic ou par les oasis bénéficiaires.
