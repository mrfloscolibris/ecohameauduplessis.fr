import React from 'react'
import { siteMetadata } from '../../gatsby-config'
import PlessisNavi from './PlessisNavi'
import SiteNavi from '../components/SiteNavi'
import SiteFooter from '../components/SiteFooter'
import emergence from 'emergence.js'
import '../sass/gatstrap.scss'
import 'animate.css/animate.css'
import 'font-awesome/css/font-awesome.css'

class Template extends React.Component {
  componentDidMount() {
    emergence.init()
  }

  componentDidUpdate() {
    emergence.init()
  }

  render() {
    const { location, children } = this.props
    return (
      <>
        <PlessisNavi {...this.props} />
        <SiteNavi title={siteMetadata.title} {...this.props} />
        {children}
        <SiteFooter />
      </>
    )
  }
}

export default Template
