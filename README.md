# Ecohameau ecohameauduplessis.fr

Site vitrine pour l'écohameau du Plessis.

## Installation

- installer [nodejs](https://nodejs.org/fr)
- installer gatsby a partir de la ligne de commande `npm i -g gatsby-cli`
- installer le dépot git `git clone git@framagit.org:mrfloscolibris/ecohameauduplessis.fr.git`
- installer les dépendances `cd ecohameauduplessis.fr;npm install`
- développer en local en lançant `gatsby develop`
